import React, { Component } from 'react'
import { View } from 'react-native'
import { Provider } from 'react-redux'
import reducers from './src/Reducers'
import { applyMiddleware, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './src/Sagas'
import AppNavigation from './src/Navigation'
import { composeWithDevTools } from 'redux-devtools-extension'
import { MenuProvider } from 'react-native-popup-menu';
const sagaMiddleware = createSagaMiddleware()

const appStore = createStore(reducers, composeWithDevTools(applyMiddleware(sagaMiddleware)))
sagaMiddleware.run(rootSaga)

export default class App extends Component {
  render () {
    return (

      <Provider store={appStore}>
          <MenuProvider>
            <View style={{flex: 1}}>
            <AppNavigation />
           </View>
          </MenuProvider>
      </Provider>
    );
  }
}

// Google maps keys - mission05 david.browning@flyfreely.io
// AIzaSyC_7FWkhe0ZHL-hHa3dJrufZDZs2GVYs7A - ios
// AIzaSyBm-zORu8I-9iD6M2lFz74y-bo6NCsgYuU - android
// <Provider store={appStore}>
// PUT /webapi/mobileapi/user/missions/MISSIONID/status
//
// {"status": "COMPLETED", "message": "SOME TEXT", "sorties": [{"number": 1, "status": "COMPLETED", "startTime": "2017-12-15T10:00:00Z", "endTime": "2017-12-15T10:00:00Z"}]}
//

// This is a io.flyfreely.portal.services.missions.UpdateMissionStatusCommand object so check there for more details




