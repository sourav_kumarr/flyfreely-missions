# Getting Started

To start the dev server and install the android dev app

```
yarn android
```

To just start the dev server:
```
yarn start
```

If you have reconnected your phone and need to redirect the debug port:
```
adb reverse tcp:8081 tcp:8081
```


# Issues building

## Android release builds

If you hit issues doing a release, that looks something like:
```
Dex: Error converting bytecode to dex:
Cause: com.android.dex.DexException: Multiple dex files define Lcom/bugsnag/BugsnagPackage;
```

The solution may be to do:
```
./gradlew clean
rm -rf app/build
```

Apparently the clean does not remove the build directory, which is where the DEX files are cached.
