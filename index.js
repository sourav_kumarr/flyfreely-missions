import { AppRegistry } from 'react-native';
import { Client } from 'bugsnag-react-native';
import App from './App';

AppRegistry.registerComponent('flyfreely-missions', () => App);

const bugsnag = new Client();