/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import {Text,View,StyleSheet,Image} from 'react-native';
import Color from '../Components/common/color';
const logoImage = require('../assets/imgs/logo.png');
const chatImage = require('../assets/imgs/chat.png');
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import { formatShortPersonName } from '../Formatters';

interface HeaderProps {
    mission: any
}

class Header extends Component<HeaderProps> {
    render() {
        const pilotName = formatShortPersonName(this.props.mission.pilot);
        const organisationName = this.props.mission.organisationName;

        return(
        <View style={styles.topContainer}>
            <View style={styles.container}>
                <Image source={require('../assets/imgs/back.png')} style={styles.headerImg} resizeMode="contain"/>
                <View style={styles.headerContainer}>
                    <View style={styles.centerChild}>
                        <Text style={[styles.textBold,styles.textColor,styles.textFont,styles.textMarginLeft]}>Missions</Text>
                    </View>
                </View>

            </View>
            <View style={styles.container}>
                <Image source={logoImage} style={styles.imageLogo} resizeMode="contain"/>
                <Text style={[styles.textColor,styles.textBold,styles.textFont3,styles.textMarginRight,styles.textMarginLeft]}>
                    {organisationName}</Text>
                <Text style={[styles.textColor,styles.textFont3]}>{pilotName}</Text>
                <View style={styles.headerContainer}>
                    <View style={styles.rightChild}>
                        <Image source={chatImage} style={[styles.rightImg,styles.textMarginRight]} resizeMode="contain"/>
                    </View>
                </View>
            </View>
        </View>
        )
  }
}
const styles = StyleSheet.create({
    topContainer:{
        flexDirection:'column',
        width:responsiveWidth(100),
        borderBottomWidth:1,
        padding:5,
        borderBottomColor:Color.borderColor
    },
    container:{
        width:responsiveWidth(100),
        flexDirection:'row'
    },

    imageLogo: {
        width: responsiveHeight(7),
        height: responsiveHeight(7),
    },
    textBold:{
        fontWeight:'bold',
        },
    textColor:{
        color:'#000'
    },
    textFont:{
        fontSize:responsiveFontSize(4),
    },
    textFont3:{
        fontSize:responsiveFontSize(3),
    },
    textMarginLeft:{
        marginLeft:responsiveWidth(5)
    },
    textMarginRight:{
        marginRight:responsiveWidth(5)
    },
    headerContainer:{
        flex:1,
        width:responsiveWidth(60)
    },
    rightChild:{
        alignItems:'flex-end',
    },
    centerChild:{
        alignItems:'center',
    },
    headerImg:{
        width:responsiveWidth(6),
        height:responsiveHeight(6),
    },
    rightImg:{
        width:responsiveWidth(7),
        height:responsiveHeight(7),
    },


});
export default Header;