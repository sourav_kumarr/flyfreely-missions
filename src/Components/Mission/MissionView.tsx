/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    Text,
    View,
    FlatList,
    Keyboard,
    PermissionsAndroid,
    Platform,
    Alert,
    TouchableOpacity,
    Image
} from 'react-native';

import MapView, { Polygon, } from 'react-native-maps';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import Icon from 'react-native-vector-icons/FontAwesome';
import Color from '../common/color';

import {
    Card,
    CardSection,
    ButtonSection,
    Button
} from '../common';

import {
    appStyles,
    screenWidth,
    screenHeight,
} from '../common/commonStyles';

import { formatDateTime, formatShortPersonName } from '../../Formatters';
import Objective from './Objective';
import Weather from './Weather';
import SortieList from './SortieList';
import { MissionState, ProgressStep, MissionResults } from '../../Domain';

const logoImage = require('../../assets/imgs/logo.png');


const ASPECT_RATIO = screenWidth / screenHeight;
const LATITUDE = -19.294805
const LONGITUDE = 146.768083
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

enum DisplayState {
    SHOW_SORTIES,
    SHOW_WEATHER,
    SHOW_OBJECTIVES
}


interface MissionViewProps {
    mission: any,
    missionState: MissionState,
    missionResults: MissionResults,
    onStartMission: () => void,
    onAbortMission: () => void,
    onEndMission: () => void,
    onStartSortie: () => void,
    onEndSortie: () => void,
    onAbortSortie: () => void
}

interface MissionViewState {
    showing: DisplayState
}

class MissionView extends Component<MissionViewProps, MissionViewState> {
    static propTypes = {
        provider: MapView.ProviderPropType
    }

    map: any;
    constructor(props) {
        super(props);

        this.state = { showing: DisplayState.SHOW_SORTIES }
    }

    onWeatherPress() {
        this.setState((state) => ({ ...state, showing: state.showing === DisplayState.SHOW_WEATHER ? DisplayState.SHOW_SORTIES : DisplayState.SHOW_WEATHER }));
    }

    onObjectivePress() {
        this.setState((state) => ({ ...state, showing: state.showing === DisplayState.SHOW_OBJECTIVES ? DisplayState.SHOW_SORTIES : DisplayState.SHOW_OBJECTIVES }));
    }

    onMapLayout() {
        if (this.props.mission) {
            this.map.fitToCoordinates(this.props.mission.location.coordinates);
        }
    }

    renderInfoPanel() {
        switch (this.state.showing) {
            case DisplayState.SHOW_OBJECTIVES:
                return <Objective mission={this.props.mission} />
            case DisplayState.SHOW_WEATHER:
                return <Weather weather={this.props.mission.weather} />
            case DisplayState.SHOW_SORTIES:
                return <SortieList sorties={this.props.missionResults.sorties} />
        }
    }

    renderMissionButtons() {
        if (this.props.mission.status == 'READY_TO_FLY' || this.props.mission.status == 'APPROVED') {
            switch (this.props.missionState.progressStep) {
                case ProgressStep.STEP_BEFORE_MISSION_START:
                    return (
                        <View style={appStyles.buttonContainer}>
                            <Button
                                onPress={this.props.onAbortMission}
                                title='Abort Mission'
                            />
                            <Button
                                title={'Start Mission'}
                                onPress={this.props.onStartMission}
                            />
                        </View>);
                case ProgressStep.STEP_MISSION_STARTED:
                    let cancelCompleteMissionButton;
                    if (this.props.missionResults.sorties.length == 0) {
                        cancelCompleteMissionButton = (<Button
                            onPress={this.props.onAbortMission}
                            title='Abort Mission'
                        />)
                    } else { 
                        cancelCompleteMissionButton = (<Button
                            onPress={this.props.onEndMission}
                            title='End Mission'
                        />)
                    }
                    return (
                        <View style={appStyles.buttonContainer}>
                            { cancelCompleteMissionButton }
                            <Button
                                title={'Start Sortie'}
                                onPress={this.props.onStartSortie}
                            />
                        </View>);
                case ProgressStep.STEP_SORTIE_STARTED:
                    return (
                        <View style={appStyles.buttonContainer}>
                            <Button
                                onPress={this.props.onAbortSortie}
                                title='Abort Sortie'
                            />
                            <Button
                                title={'End Sortie'}
                                onPress={this.props.onEndSortie}
                            />
                        </View>);
                case ProgressStep.STEP_MISSION_COMPLETED:
                    return null;
            }
        } else {
            return null;
        }
    }

    render() {
        const {
            name,
            missionDate,
            craft
        } = this.props.mission;

        const coordinate = {
            latitude: this.props.missionState.location.latitude,
            longitude: this.props.missionState.location.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
        };

        const infoPanel = this.renderInfoPanel();

        const missionButtons = this.renderMissionButtons();

        return (
            <View style={appStyles.paddedContainer}>
                <View style={styles.mapContainer}>
                    <MapView
                        ref={ref => { this.map = ref }}
                        provider={this.props.provider}
                        style={styles.map}
                        showsUserLocation={true}
                        mapType="satellite"
                        // scrollEnabled={false}
                        // zoomEnabled={false}
                        // rotateEnabled={false}
                        // pitchEnabled={false}
                        initialRegion={coordinate}
                        onMapReady={this.onMapLayout.bind(this)}
                    >
                        <Polygon
                            coordinates={this.props.mission.location.coordinates}
                            strokeColor={Color.mapLineColor}
                            fillColor="transparent"
                            strokeWidth={2}
                            lineDashPhase={3}
                        />

                    </MapView>
                    <View style={appStyles.topAndBottom}>
                        <View style={appStyles.row}>
                            <View style={styles.bubble}>
                                <Text>{this.props.mission.location.name}</Text>
                                <View style={this.props.missionState.isLocationVerified ? appStyles.verifiedStatus : appStyles.nonVerifiedStatus} />
                            </View>
                        </View>
                        <View style={appStyles.row}>
                            <Button
                                style={appStyles.smallButton}
                                title='Weather'
                                onPress={() => this.onWeatherPress()}
                            />
                            <Button
                                style={appStyles.smallButton}
                                title={'Objectives'}
                                onPress={() => this.onObjectivePress()}
                            />
                        </View>
                    </View>
                </View>


                {infoPanel}

                {missionButtons}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mapContainer: {
        width: screenWidth - 20,
        height: responsiveHeight(40),
        marginVertical: 10,
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },

    bubble: {
        backgroundColor: 'rgba(255,255,255,0.7)',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        flex: 0
    },
});

export default MissionView;