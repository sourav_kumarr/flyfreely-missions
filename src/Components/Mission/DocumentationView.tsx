/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    Text,
    View,
    FlatList,
    Keyboard,
    PermissionsAndroid,
    Platform,
    Alert,
    Button,
    TouchableOpacity,
    Image
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import {
    Card,
    CardSection,
    ButtonSection
} from '../common';

import {
    appStyles
} from '../common/commonStyles';

import { formatDateTime, formatShortPersonName } from '../../Formatters';

import FormList from './FormList';
import DocumentList from './DocumentList';
import { MissionResults } from '../../Domain';

const logoImage = require('../../assets/imgs/logo.png');
interface MissionOverviewProps {
    mission: any,
    missionResults: MissionResults
}

const DocumentationView = (props: MissionOverviewProps): JSX.Element => {
    const localMissionDate = formatDateTime(props.mission.missionDate)
    const pilotName = formatShortPersonName(props.mission.pilot);
    const organisationName = props.mission.organisationName; 
    
    const onSelectChecklist = () => {};

    return (
        <View style={appStyles.paddedContainer}>
            <FormList mission={props.mission} missionResults={props.missionResults} onSelectChecklist={this.onSelectChecklist}></FormList>
            <DocumentList documents={props.mission.documents} missionId={props.mission.id}></DocumentList>
        </View>
    )
}

export default DocumentationView;