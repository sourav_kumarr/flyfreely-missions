/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import {
    Text,
    View, ListView, TouchableOpacity,
} from 'react-native';

import { connect } from 'react-redux';
import {
    Card,
    CardSection,
} from '../common';
import * as actions from '../../Actions'
import { appStyles } from '../../Components/common/commonStyles';
import Icon from 'react-native-vector-icons/FontAwesome';
import { CircularProgress, AnimatedCircularProgress } from 'react-native-circular-progress';
import { Button } from "../common/Button";
import { CurrentDownload, StoredDocument } from '../../Reducers/DocumentListReducer';
import { MobileAppState } from '../../Reducers';
import { getMissionDocuments, getMissionDocumentStatuses } from '../../Selectors';

interface DocumentListProps {
    missionId: number,
    documents: any,
    documentList: Array<StoredDocument>,
    currentDownload: CurrentDownload
}

interface ReduxActions {
    viewDocument: (missionId: number, documentId: number) => any,
    filterDocuments: (documents: any) => any,
    downloadDocument: (missionId: number, documentId: number) => any,
    downloadSingleFile: (missionId: number, documentId: number) => any
}
interface DocumentListState {
    dataSource: any
}

class DocumentList extends Component<DocumentListProps & ReduxActions, DocumentListState> {
    viewDocument(documentId) {
        this.props.viewDocument(this.props.missionId, documentId);
    }
    filterDocuments(docs) {
        this.props.filterDocuments(docs);
    }
    downloadDocument(item) {
        // alert('download clicked');
        console.log(item)
        this.props.downloadSingleFile(this.props.missionId, item.id);
        // this.setState({dataSource:this.state.dataSource.cloneWithRows(this.props.documentList)});
    }
    downloadAllDocuments() {
        // alert('download all is called');
        this.props.documentList.map((item, index) => {
            if (!item.path)
                this.props.downloadSingleFile(this.props.missionId, item.id);
        });
    }

    renderDownloadAll() {
        if (this.isDownloaded()) {
            return (
                <Button
                    title="Download All"
                    onPress={this.downloadAllDocuments.bind(this)}
                    style={appStyles.marginTop_2}
                />
            )
        }
        return null;

    }

    isDownloaded() {
        let documents = this.props.documentList;
        for (let i = 0; i < documents.length; i++) {
            if (!documents[i].path) {
                return true;
            }
        }
        return false;
    }

    renderDocumentLinks() {
        if (this.props.documentList) {
            if (this.props.documentList.length > 0) {

                return (
                    <View style={[appStyles.mainContainer]}>
                        <ListView
                            dataSource={this.state.dataSource.cloneWithRows(this.props.documentList)}
                            renderRow={this.renderDocumentData.bind(this)}
                        />

                        {this.renderDownloadAll()}
                    </View>

                )
            }
            else {
                return (<Text>No documents attached</Text>);
            }
        }
        else {
            return (<Text>No documents attached</Text>);
        }

    }

    componentWillMount() {
        if (this.props.documents.length > 0) {
            this.props.filterDocuments(this.props.documents);

        }
        let dataSource = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.setState({ dataSource: dataSource });
    }
    render() {
        const documents = this.renderDocumentLinks();

        return (<View>
            <Text style={appStyles.sectionHeading}>Documents</Text>
            {documents}
        </View>);
    }

    renderDocumentData(item) {
        if (item.path) {
            return (
                <View style={[appStyles.row, appStyles.padding_2]}>
                    <Text onPress={this.viewDocument.bind(this, item.id)} style={appStyles.documentLink}>{item.name}</Text>
                    <Text>Exists</Text>
                </View>
            )
        }
        else if (item.downloading) {
            return (
                <View style={[appStyles.row]}>
                    <Text style={appStyles.documentLink}>{item.name}</Text>
                    <AnimatedCircularProgress
                        size={25}
                        width={3}
                        fill={item.progress}
                        tintColor="#00e0ff"
                        backgroundColor="#3d5875">
                        {
                            (fill) => (
                                <Text style={{ color: 'black', fontSize: 8 }}>
                                    { item.progress.toFixed(0) }%
                                </Text>

                            )
                        }
                    </AnimatedCircularProgress>
                </View>
            )
        }
        else {
            return (
                <View style={[appStyles.row, appStyles.padding_2]}>
                    <Text style={appStyles.documentLink}>{item.name}</Text>
                    <TouchableOpacity onPress={this.downloadDocument.bind(this, item)}>
                        <Icon
                            name="download"
                            size={20}
                            style={{ color: 'grey' }}
                        />
                    </TouchableOpacity>
                </View>
            )
        }

    }
}


const mapStateToProps = (state: MobileAppState, ownProps: DocumentListProps) => (state) => {
    const { currentDownload } = state.DocumentListReducer;
    return { 
        currentDownload,
        documentList: getMissionDocumentStatuses(state, ownProps.missionId),
    }
}

export default connect(mapStateToProps, actions)(DocumentList);