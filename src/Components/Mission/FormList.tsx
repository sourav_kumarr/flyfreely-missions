/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
// Note, this should be renamed to documentation at some point.

import React, { Component } from 'react';
import { 
  StyleSheet,
  LayoutAnimation, 
  Text, 
  TouchableWithoutFeedback, 
  TouchableOpacity,
  View,
} from 'react-native';

import { connect } from 'react-redux';
import moment from 'moment';

import { 
  Card, 
  CardSection,
  Button,
} from '../common';
import { ExpandingPanel } from '../ExpandingPanel';
import { ChecklistResponses } from '../ChecklistResponses';
import * as actions from '../../Actions';
import Color from '../common/color';
import {appStyles} from '../../Components/common/commonStyles';
import { MissionResults } from '../../Domain';

interface FormListProps {
    onSelectChecklist: any,
    mission: any,
    missionResults: MissionResults
}

interface FormListState {
    ix?: number,
    step?: string
}

const checkResponses = (checklist, responseList) => {
    if (!responseList) {
        return {responses: [], success: false};
    }

    const responseObject = responseList.find((r) => r.checklistVersionId === checklist.id);
    if (!responseObject) {
        return {responses: [], success: false};
    }
    // TODO validate list responses
    return {responses: responseObject.values, success: true};
};

class FormList extends Component<FormListProps, FormListState> {
    constructor(props) {
        super(props);
        this.state = {};
    }

    decodeStep(encodedStep) {
        const [entity, step] = encodedStep.split('\.');
        return {entity, step}
    }

    isExpanded(step, ix) {
        return this.state.ix === ix && this.state.step === step;
    }

    onSelectChecklist(step, ix) {
        if (this.isExpanded(step, ix)) {
            this.setState({step: null, ix: null});
        } else {
            this.setState({step, ix});
        }
    }

    renderChecklists(checklists, step) {
        return checklists.map((checklist, ix) => {
            const expanded = this.isExpanded(step, ix);
            const decodedStep = this.decodeStep(step);
            const responseList = this.props.missionResults.checklistResponses[decodedStep.step];
            const {responses, success} = checkResponses(checklist, responseList);
        
            const content = <ChecklistResponses checklist={checklist} responses={responses}/>
            return <ExpandingPanel key={ix} title={checklist.checklistName} expanded={expanded}
                        success={success} 
                        content={content}
                        onPress={this.onSelectChecklist.bind(this, step, ix)}></ExpandingPanel>
        });
    }

    renderStep(key, title) {
        return (<View key={key}><Text style={appStyles.itemTitle}>{title}</Text>
            {this.renderChecklists(this.props.mission.checklists[key], key)}
            </View>);
    }

    renderSteps() {
        return [['Mission.pre-mission', 'Pre-Mission'], ['Mission.post-mission', 'Post-Mission']]
            .map(item => this.renderStep(item[0], item[1]))
    }

    render() {
        return (<View>
            <Text style={appStyles.sectionHeading}>Forms and checklists</Text>
            {this.renderSteps()}
        </View>);
    }
}


const mapStateToProps = state => ({})

export default FormList;