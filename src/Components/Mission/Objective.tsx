/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import {
    StyleSheet,
    LayoutAnimation,
    Text,
    TouchableWithoutFeedback,
    TouchableOpacity,
    View,
} from 'react-native';

import { connect } from 'react-redux';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions'

import moment from 'moment';
import {
    Card,
    CardSection,
    Button,
} from '../common';
import * as actions from '../../Actions'
import Color from '../common/color';
import { appStyles } from '../../Components/common/commonStyles';

interface ObjectiveProps {
    mission: any
}

const Objective = (props: ObjectiveProps): JSX.Element => (
    <CardSection>
      <Text style={appStyles.textFiledName}>Mission Objective : </Text>
      <Text >{(props.mission.missionObjective ===null)?'NA':props.mission.missionObjective}</Text>
    </CardSection>)

export default Objective;