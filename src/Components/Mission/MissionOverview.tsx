/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    Text,
    View,
    FlatList,
    Keyboard,
    PermissionsAndroid,
    Platform,
    Alert,
    Button,
    TouchableOpacity,
    Image
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import {
    Card,
    CardSection,
    ButtonSection
} from '../common';

import {
    appStyles
} from '../common/commonStyles';

import { IconButton } from '../common/IconButton';

import { formatDateTime, formatShortPersonName } from '../../Formatters';

import { MissionState } from '../../Domain';

const logoImage = require('../../assets/imgs/logo.png');
interface MissionOverviewProps {
    mission: any,
    missionState: MissionState,
    isDocsOpen: boolean,
    onToggleDocumentation: () => void
}

const MissionOverview = (props: MissionOverviewProps): JSX.Element => {
    const localMissionDate = formatDateTime(props.mission.missionDate)
    const pilotName = formatShortPersonName(props.mission.pilot);
    const organisationName = props.mission.organisationName;

    const docsIcon = props.isDocsOpen ? "folder-open" : "folder";
    
    return (
        <View style={appStyles.paddedContainer}>
            <CardSection>
                <Image source={logoImage} style={appStyles.imageLogo} resizeMode="contain" />
                <Text style={[appStyles.textColor, appStyles.textBold, appStyles.textMarginRight, appStyles.textMarginLeft]}>
                    {organisationName}</Text>
                <Text style={[appStyles.textColor]}>{pilotName}</Text>

                <View style={appStyles.rightContainer}>
                    <IconButton onPress={props.onToggleDocumentation} icon={docsIcon} />
                    {/* <View style={appStyles.rightChild}>
                        <Icon name="comments" size={30} />
                    </View> */}
                </View>
            </CardSection>
            <CardSection>
                <Text style={[appStyles.textValue, appStyles.textBold]}>{props.mission.name}</Text>
                <Text style={[appStyles.textValue, appStyles.alignRight]}>{localMissionDate}</Text>
            </CardSection>
            <CardSection>
                <Text style={appStyles.textFiledName}>Craft : </Text>
                <View style={appStyles.row}>
                    <Text style={appStyles.textValue}>{props.mission.craft.nickname}</Text>
                    <View style={props.missionState.isNfcVerified ? appStyles.verifiedStatus : appStyles.nonVerifiedStatus} />
                </View>
            </CardSection>
            <CardSection>
                <Text style={appStyles.textFiledName}>Payload : </Text>
                <Text style={appStyles.textValue}>NA</Text>
                <Text>{props.mission.missionType.name}</Text>
                <Text>{props.mission.status}</Text>
            </CardSection>
        </View>
    )
}

const styles = StyleSheet.create({

});

export default MissionOverview;