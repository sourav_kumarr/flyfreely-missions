/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import { 
  StyleSheet,
  Text, 
  View,
} from 'react-native';

import { CardSection } from '../common';
import Color from '../common/color';


export default class SortieListHeader extends Component {

  render() {
    return (
      <CardSection
        style={styles.container}
      >
        <View style={styles.padding}/>
        <Text style={styles.textTime}>Start</Text>
        <Text style={styles.textTime}>End</Text>
        <Text style={styles.textStatus}>Status</Text>
      </CardSection>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderBottomColor: Color.borderColor,
  },
  padding: {
    flex: 1,
  },
  textTime: {
    flex: 1,
    fontSize: 16,
    fontWeight: 'bold',
    color: Color.defaultTextColor,
  },
  textStatus: {
    flex: 2,
    fontSize: 16,
    fontWeight: 'bold',
    color: Color.defaultTextColor,
  },
});
