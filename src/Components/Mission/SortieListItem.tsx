/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import { 
  StyleSheet,
  Text, 
  View,
} from 'react-native';

import { connect } from 'react-redux';
import moment from 'moment';
import { CardSection} from '../common';

import Color from '../common/color';

interface SortieListItemProps {
  number: number,
  status: string,
  startTime: string,
  endTime: string
}


export default class SortieListItem extends Component<SortieListItemProps> {

  leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
  }

  render() {
    const {
      number, 
      status,
      startTime,
      endTime,
    } = this.props

    const localStartTime = moment.utc(startTime).local().format("HH:mm");
    let localEndTime = '';
    if (endTime) {
      localEndTime = moment.utc(endTime).local().format("HH:mm");
    }
    
    return (
      <CardSection>
        <Text style={styles.textNumber}>#{this.leftPad(number, 2)}</Text>
        <Text style={styles.textTime}>{localStartTime}</Text>
        <Text style={styles.textTime}>{localEndTime}</Text>
        <Text style={styles.textStatus}>{this.capitalizeFirstLetter(status)}</Text>
      </CardSection>
    )
  }
}


const styles = StyleSheet.create({
  textNumber: {
    flex: 1,
    textAlign: 'center',
    fontSize: 18,
    color: Color.defaultTextColor,
    fontWeight: 'bold',
  },
  textTime: {
    flex: 1,
    fontSize: 14,
    color: Color.defaultTextColor,
  },
  textStatus: {
    flex: 2,
    fontSize: 14,
    color: Color.defaultTextColor,
  },
});
