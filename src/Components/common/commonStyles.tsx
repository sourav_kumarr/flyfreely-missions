/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import {
    StyleSheet,
    Dimensions,
    Platform,
} from 'react-native';
import Color from './color';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export const { width: screenWidth, height: screenHeight } = Dimensions.get('window');

export const appStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.defaultBackgroundColor,
        padding: 5,
        alignItems: 'center',
    },
    sectionHeading: {
        fontSize: responsiveFontSize(3),
        fontWeight: 'bold',
    },
    itemTitle: {
        fontSize: responsiveFontSize(2),
        fontWeight: 'bold',
    },
    paddedContainer: {
        width: responsiveWidth(95)
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    fill: {
        flex: 1
    },
    textFiledName: {
        fontSize: 16,
        color: Color.defaultTextColor,
        fontWeight: 'bold',
        // width: 90,
    },
    textValue: {
        fontSize: 16,
        color: Color.defaultTextColor,
    },
    textBold: {
        fontWeight: 'bold',
    },
    headerImg:{
        width: responsiveWidth(6),
        height: responsiveHeight(6),
    },
    headerContainer:{
        flex:1,
        width:responsiveWidth(60)
    },    
    textColor:{
        color:'#000'
    },
    rightChild:{
        alignItems: 'flex-end'
    },
    centerChild:{
        alignItems:'center',
    },
    lastChild: {
        alignSelf: 'flex-end'
    },
    textMarginLeft:{
        marginLeft:responsiveWidth(5)
    },
    textMarginRight:{
        marginRight:responsiveWidth(5)
    },
    imageLogo: {
        width: responsiveHeight(7),
        height: responsiveHeight(7),
    },
    buttonContainer:{
        flexDirection: 'row',
        width: responsiveWidth(95),
        marginTop: 3,
        justifyContent: 'space-between',
    },
    topAndBottom: {
        ...StyleSheet.absoluteFillObject,
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    leftContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    rightContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    nonVerifiedStatus: {
        marginHorizontal: 5,
        backgroundColor: '#ff0000',
        width: 15,
        height: 15,
        borderRadius: 7.5,
    },
    verifiedStatus: {
        marginHorizontal: 5,
        backgroundColor: '#00ff00',
        width: 15,
        height: 15,
        borderRadius: 7.5,
    },
    secondaryButton: {
        backgroundColor: '#ffffff',
        color: '#eeeeee'
    }, 
    success: {
        color: '#00ff00'
    }, 
    failure: {
        color: '#ff0000'
    },
    smallButton: {
        flex: 0,
        marginHorizontal: 0
    },
    documentLink: {
        color: '#007DD5',
        textDecorationLine: 'underline'
    },
    alignRight: {
        textAlign: 'right',
    },
    padding_2:{
        padding:responsiveWidth(2)
    },
    marginRight_10:{
        marginRight:responsiveWidth(10)
    },
    marginTop_2:{
        marginTop:responsiveHeight(2)
    },
    marginRight_15:{
        marginRight:responsiveWidth(15)
    },
    mainContainer: {
        flex: 1,
        backgroundColor: Color.defaultBackgroundColor,
    },
});