/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component, StatelessComponent } from 'react'
import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Image,
    Easing
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import * as Animatable from 'react-native-animatable';

import PropTypes from 'prop-types';

import Color from './color';

interface IconButtonProps {
    style?: any,
    icon: string,
    animated?: boolean,
    onPress: () => void
}

class IconComponent extends Component<IconButtonProps> {
    render() {
        return (
            <Icon size={30} name={this.props.icon} />
        );
    }
};

const AnimatedIcon = Animatable.createAnimatableComponent(IconComponent);

const IconButton = (props: IconButtonProps) => {
    const icon = <AnimatedIcon icon={props.icon} animation={props.animated ? 'rotate' : ''} iterationCount="infinite" useNativeDriver easing="0"/> ;
    return (
        <TouchableOpacity
            style={[styles.container, props.style]}
            onPress={props.onPress}
        >
            { icon }
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 10,
    },
});

export { IconButton }
