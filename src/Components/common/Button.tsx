/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react'
import { 
  StyleSheet,
  View,
  Text, 
  TouchableOpacity,
} from 'react-native'
import PropTypes from 'prop-types';

import Color from './color';

interface ButtonProps {
  style?: any,
  titleStyle?: any,
  title: string,
  onPress: () => void
}

const Button = (props: ButtonProps) => (
      <TouchableOpacity
        onPress={props.onPress}
        style={[styles.container, props.style]}
      >
        <Text style={[styles.textTitle, props.titleStyle]}>{props.title}</Text>
      </TouchableOpacity>
    ); 


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#fff',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Color.buttonColor,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10
  },
  textTitle: {
    textAlign: 'center',
    color: Color.buttonColor,
    fontSize: 16,
    fontWeight: '600',
    marginVertical: 10,
    marginHorizontal: 5,
  },

});

export { Button }
