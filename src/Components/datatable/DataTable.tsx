/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react'
import {
    Image,
    View,
    ListView,
    Text,
    Dimensions,
    TouchableOpacity,
    Linking,
    LayoutAnimation,
    Alert
} from 'react-native';
import PropTypes  from 'prop-types'
import Color from '../common/color'
import Style from './style';
import Cell from './Cell';
import HeaderCell from './HeaderCell';

interface DataTableProps {
    containerStyle: string,
    headerStyle: string, 
    headerCellStyle: any,
    headerHighlightColor: any,
    cellStyle: any,
    fields: any,
    dataSource: any,
    onSort: (field: string, ascending: boolean) => void,
    renderHeaderCell: (field: any, ix: number) => void,
    renderRow?: (row: any) => any
}

interface DataTableState {
    sortedField: any,
    isAscending: boolean
}


class DataTable extends Component<DataTableProps, DataTableState> {
    static defaultProps: Partial<DataTableProps> = {
        headerHighlightColor: Color.buttonColor
    }

    constructor(props) {
        super(props);

        this.state = {
            sortedField: null,
            isAscending: true
        }
    }

    render() {
        return (
            <View style={[Style.container, this.props.containerStyle]}>
                {this.renderHeader()}
                {this.renderTable()}
            </View>
        )
    }

    renderHeader() {
        return (
            <View style={[Style.header, this.props.headerStyle]}>
                {this.props.fields.map(this.renderHeaderCell.bind(this))}
            </View>
        );
    }

    renderHeaderCell(field, i) {
        if (this.props.renderHeaderCell) {
            return this.props.renderHeaderCell(field, i);
        }

        return (
            <HeaderCell
                key={i}
                style={this.props.headerCellStyle}
                highlightColor={this.props.headerHighlightColor}
                onSort={this.onSort.bind(this)}
                isSortedField={this.state.sortedField === field}
                isSortedAscending={this.state.isAscending}
                field={field} />
        )
    }

    renderTable() {
        return (
            <ListView
                dataSource={this.props.dataSource}
                renderRow={this.props.renderRow}
                enableEmptySections={true}
                />
        );
    }

    renderRow(row, sectionId, rowId) {
        if (!this.props.renderRow) {
            return;
        }

        this.props.renderRow(row);
    }

    renderCells(row) {
        var keys = Object.keys(row);

        return this.props.fields.map((field, index) => {
            var key = keys[index];

            var value = row[key].toString();

            return (
                <Cell
                    key={index}
                    style={this.props.cellStyle}
                    label={value} />
            );
        });
    }

    onSort(field, isAscending) {
        if (!this.props.onSort) {
            return;
        }

        this.setState({
            sortedField: field,
            isAscending: isAscending
        });
        this.props.onSort(field, isAscending);
    }
}


// DataTable.propTypes = {
//     fields: PropTypes.arrayOf(PropTypes.shape({
//         label: PropTypes.string.isRequired,
//         sortable: PropTypes.bool
//     })),
//     onSort: PropTypes.func,
//     dataSource: PropTypes.object.isRequired,
//     containerStyle: PropTypes.number,
//     renderHeaderCell: PropTypes.func,
//     headerStyle: PropTypes.number,
//     headerCellStyle: PropTypes.number,
//     headerHighlightColor: PropTypes.string,
//     cellStyle: PropTypes.number,
//     renderRow:PropTypes.func

// }

export default DataTable;