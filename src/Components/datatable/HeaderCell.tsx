/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react'
import {
    View,
    Text,
    Dimensions,
    TouchableHighlight
} from 'react-native';
import PropTypes  from 'prop-types'

import Icon from 'react-native-vector-icons/FontAwesome';

import Style from './style';

interface HeaderCellProps {
    field: any,
    highlightColor: any,
    style: any,
    isSortedField: boolean,
    isSortedAscending: boolean,
    onSort: (field: any, isAscending: boolean) => void
}

class HeaderCell extends Component<HeaderCellProps> {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableHighlight
                onPress={() => this.onPress()}
                disabled={!this.props.field.sortable}
                underlayColor={this.props.highlightColor}
                style={{ flex: 1, height: 40 }}>
                <Text style={[Style.cell, Style.headerCell, this.props.style]}>
                    {this.props.field.label}
                    {this.renderSortIcons()}
                </Text>
            </TouchableHighlight>
        );
    }

    renderSortIcons() {
        if (!this.props.field.sortable) {
            return null;
        }

        if (!this.props.isSortedField) {
            return <Icon name="sort" />
        } else if (this.props.isSortedAscending) {
            return <Icon name="sort-down" />
        } else {
            return <Icon name="sort-up" />
        }
    }

    getCellStyle(isUp) {
        if (!this.props.isSortedField) {
            return Style.unsortedCell;
        }

        if (isUp) {
            return !this.props.isSortedAscending ? Style.sortedCell : null
        } else {
            return this.props.isSortedAscending ? Style.sortedCell : null
        }
    }

    onPress() {
        const newSorting = !this.props.isSortedField ? true : !this.props.isSortedAscending;
        this.props.onSort && this.props.onSort(this.props.field, newSorting);
    }
}


export default HeaderCell;