/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        padding: 5
    },
    cell: {
        flex: 1,
        borderColor: 'lightgrey',
        borderBottomWidth: 1,
        padding: 2
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerCell: {
        flex: 1,
        textAlign: 'center',
        lineHeight: 30,
        fontWeight: 'bold'
    },
    row: {
        flex: 1,
        flexDirection: 'row',
    },
    altRow: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'lightblue'
    },
    contentCell: {
    },
    sortedCell: {
        color: 'grey'
    },
    unsortedCell: {
        color: 'grey'
    },
    rtfColor:{
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#EDF5E9'

    },
    approvedColor:{
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#FDF2E6'
   },
    planned:{
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#FAE5E5'

    }
});