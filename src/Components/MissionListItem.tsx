/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import { 
  StyleSheet,
  LayoutAnimation, 
  Text, 
  TouchableWithoutFeedback, 
  TouchableOpacity,
  View,
} from 'react-native';

import { connect } from 'react-redux';
import moment from 'moment';
import { 
  Card, 
  CardSection,
  Button,
} from './common';
import * as actions from '../Actions'
import Color from './common/color';

interface MissionListItemProps {
  id: number,
  expanded: boolean,
  location: any,
  craft: any,
  status: string,
  missionDate: string,
  organisationName: string,
  name: string,
  selectMission: (missionId: number) => void,
  onStart: () => void
}


class MissionListItem extends Component<MissionListItemProps> {

  componentWillUpdate () {
    LayoutAnimation.spring()
  }

  onStartMission() {
    if (this.props.onStart) {
      this.props.onStart();
    }
  }

  get renderMissionDetails() {
    const {
      id,
      expanded,
      location,
      craft,
      status,
    } = this.props;

    if (expanded) {
       return (
        <View style={styles.moreDetailsContainer}>
          <View style={styles.moreDetailsLeftContainer}>
            <CardSection>
              <Text style={styles.textFiledName}>Location : </Text>
              <Text style={styles.textValue}>{location.name}</Text>
            </CardSection>
            <CardSection>
              <Text style={styles.textFiledName}>Craft : </Text>
              <Text style={styles.textValue}>{craft.nickname}</Text>
            </CardSection>
          </View>
          { status !== 'COMPLETED' && 
            <View style={styles.moreDetailsRightContainer}>
              <Button 
                title="Start"
                style={styles.buttonStart}
                onPress={() => this.onStartMission()}
              />
            </View>
          }
        </View>
      )
    } 
  }

  render() {
    const {
      id, 
      name,
      missionDate,
      status,
      organisationName,
    } = this.props

    const localMissionDate = moment.utc(missionDate).local().format("YYYY-MM-DD HH:mm");
    
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => this.props.selectMission(id)}
        activeOpacity={0.6}
      >
        <CardSection>
          <Text style={[styles.textValue, styles.textBold]}>{name}</Text>
          <Text style={styles.textValue}>{status}</Text>
        </CardSection>
        <CardSection>
          <Text style={styles.textValue}>{localMissionDate}</Text>
          <Text style={styles.textValue}>{organisationName}</Text>
        </CardSection>

        {this.renderMissionDetails}
      </TouchableOpacity>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    marginVertical: 5,
    marginHorizontal: 10,
    borderWidth: 1,
    borderColor: Color.borderColor,
    borderRadius: 3,
  },
  textFiledName: {
    fontSize: 16,
    color: Color.defaultTextColor,
    fontWeight: 'bold',
    width: 90,
  },
  textValue: {
    flex: 1,
    fontSize: 16,
    color: Color.defaultTextColor,
  },
  textBold: {
    fontWeight: 'bold',
  },

  moreDetailsContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  moreDetailsLeftContainer: {
    flex: 4,
  },
  moreDetailsRightContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    padding: 5,
  },
  buttonStart: {
  },
});


const mapStateToProps = (state, ownProps) => {
  const expanded = state.MissionSelectionReducer === ownProps.id

  return { expanded }
}

export default connect(mapStateToProps, actions)(MissionListItem)