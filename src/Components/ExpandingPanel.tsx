/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import {
    StyleSheet,
    LayoutAnimation,
    Text,
    TouchableWithoutFeedback,
    TouchableOpacity,
    View,
} from 'react-native';

import { connect } from 'react-redux';
import moment from 'moment';

import Icon from 'react-native-vector-icons/FontAwesome';

import {
    Card,
    CardSection,
    Button,
} from './common';
import * as actions from '../Actions'
import Color from './common/color';
import { appStyles } from '../Components/common/commonStyles';

interface ExpandingPanelProps {
    expanded: boolean,
    success?: boolean,
    onTouch: () => void
}

const ExpandingPanel = (props) => {
    const toggle = props.expanded ? <Icon name="angle-down" size={30} /> : <Icon name="angle-right" size={30} />
    const content = props.expanded ? props.content : undefined;
    const successFailure = props.success === undefined ? undefined : props.success ? <Icon name="check" style={appStyles.success} size={30} /> : <Icon name="times" style={appStyles.failure} size={30} />
    return (
        <View>
            <TouchableOpacity style={appStyles.row} onPress={props.onPress}>
                <Text style={appStyles.fill}>{props.title}</Text>
                <Text>{toggle}</Text>
                {successFailure}
            </TouchableOpacity>
            {content}
        </View>);
}

export { ExpandingPanel };