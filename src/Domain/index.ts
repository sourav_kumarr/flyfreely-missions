/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
/**
 * This describes the current state of a mission
 */
export interface MissionState {
    isNfcSupported: boolean,
    isNfcVerified: boolean,
    isLocationVerified: boolean,
    isVerificationOverridden: boolean,
    progressStep: ProgressStep,
    location: GPSPosition
}

interface GPSPosition {
    latitude: number,
    longitude: number
}

export enum ProgressStep {
    STEP_BEFORE_MISSION_START,
    STEP_MISSION_STARTED,
    STEP_SORTIE_STARTED,
    STEP_MISSION_COMPLETED
}

export interface MissionResults {
    id: number,
    sorties: any,
    checklistResponses: Array<any>,
    formResponses: Array<any>,
    status: string
}

export const SORTIE_NEW = 'NEW';
export const SORTIE_STARTED = 'STARTED';
export const SORTIE_FLYING = 'FLYING'
export const SORTIE_ABORTED = 'ABORTED';
export const SORTIE_COMPLETED = 'COMPLETED';

export const MISSION_FLYING = 'FLYING';
export const MISSION_COMPLETED = 'COMPLETED';
export const MISSION_CANCELLED = 'CANCELLED';