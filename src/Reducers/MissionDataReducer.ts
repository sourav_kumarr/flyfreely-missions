/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import {
    MISSIONS_CHANGED,
    POST_MISSION_DATA,
    POST_MISSION_DATA_FAILED,
    REFRESH_MISSIONS,
    POST_MISSION_DATA_COMPLETED,
    STORE_SORTIE,
    STORE_CURRENT_LOCATION,
    ORG_CHANGED,
    ORG_SELECTED,
    ORG_REFRESHED,
    STORE_MISSION
} from '../Actions/ActionTypes';

import { MissionResults } from '../Domain';

interface SyncableMissionResults extends MissionResults {
    dirty: boolean
}
interface MissionData {
    syncing: boolean,
    error: string,
    currentLocation: {},
    missionResults: SyncableMissionResults[]
}

const INITIAL_STATE = {
    syncing: false,
    error: null,
    currentLocation: {},
    missionResults: []
}

const reduceMissionData = (record, payload) => ({
        ...record,
        status: payload.status,
        dirty: true,
        checklistResponses: {...record.checklistResponses, [payload.step]: payload.checklistResponses},
        formResponses: {...record.formResponses, [payload.step]: payload.formResponses}
    });

export default (state: MissionData = INITIAL_STATE, action): MissionData => {
    switch (action.type) {
        case MISSIONS_CHANGED:
            const existingIds = state.missionResults.map((record) => record.id)
            const newIds = action.payload.missionList.map((record) => record.id).filter((id) => existingIds.indexOf(id) === -1);

            if (newIds.length == 0) {
                return state;
            }
            return {
                ...state,
                missionResults: [...state.missionResults, ...newIds.map((id) => ({ id: id, sorties: [], checklistResponses: {}, formResponses: {}, dirty: false }))]
            }

        case STORE_MISSION:
            return {
                ...state,
                missionResults: state.missionResults.map((m) => m.id === action.payload.missionId ? reduceMissionData(m, action.payload) : m)
            }
        case STORE_SORTIE:
            return {
                ...state,
                missionResults: state.missionResults.map((m) => m.id === action.payload.missionId
                    ? { ...m, dirty: true, sorties: m.sorties.concat(action.payload.sortie) }
                    : m)
            }
        case POST_MISSION_DATA:
            return {
                ...state,
                syncing: true,
                error: null,
            }
        case POST_MISSION_DATA_COMPLETED:
            return {
                ...state,
                syncing: false,
                error: null,
            }
        case POST_MISSION_DATA_FAILED:
            return {
                ...state,
                syncing: false,
                error: action.payload,
            }
        case STORE_CURRENT_LOCATION:
            return {
                ...state,
                currentLocation: action.payload,
            }
        default:
            return state
    }
};