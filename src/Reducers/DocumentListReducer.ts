/**
 * Created by SachTech on 23-05-2018.
 */
import {
    DocumentListActions, MISSIONS_CHANGED
}
    from '../Actions/ActionTypes'
import { DocumentListActionTypes } from '../Actions';

/**
 * This contains the additional information stored in the redux store
 */
export interface StoredDocument {
    id: number,
    path: string,
    progress: number,
    downloading: boolean
}

export interface CurrentDownload {
    _id: number,
    error: string,
    progress: number,
    loading: boolean
}

export interface DocumentListState {
    documentList: Array<StoredDocument>
    currentDownload: CurrentDownload
}

const INITIAL_STATE = {
    documentList: [],
    currentDownload: { loading: false, _id: null, error: null, progress: 0 }
}



export default (state: DocumentListState = INITIAL_STATE, action: DocumentListActionTypes): DocumentListState => {
    switch (action.type) {
        case MISSIONS_CHANGED:
            const existingIds = state.documentList.map((record) => record.id)
            const newIds = [].concat(...action.payload.missionList.map((mission) => mission.documents.map(d => d.id)))
                .filter((id) => existingIds.indexOf(id) === -1);

            if (newIds.length == 0) {
                return state;
            }
            return {
                ...state,
                documentList: [...state.documentList, ...newIds.map((id) => ({ id: id, downloading: false, progress: 0, path: null }))]
            }
        case DocumentListActions.DOWNLOAD_FILE_PROGRESS:
            console.log('progress ' + action.payload.progress + " % \n");

            return {
                ...state,
                documentList: state.documentList.map(d => d.id == action.payload._id ? { ...d, downloading: true, progress: action.payload.progress } : d)

            };
        case DocumentListActions.DOWNLOAD_FILE_FINISHED:
            return {
                ...state,
                documentList: state.documentList.map(d => d.id == action.payload._id ? { ...d, downloading: false, path: action.payload.path } : d)
            };
        case DocumentListActions.DOWNLOAD_FILE_ERROR:
            console.log('error ' + action.payload.error + "\n");
            return {
                ...state,
                documentList: state.documentList.map(d => d.id == action.payload._id ? { ...d, downloading: false } : d)
            };

        default:
            return state
    }
}