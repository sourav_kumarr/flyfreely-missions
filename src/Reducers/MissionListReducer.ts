/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import {
    MISSIONS_CHANGED,
    POST_MISSION_DATA,
    POST_MISSION_DATA_FAILED,
    REFRESH_MISSIONS,
    POST_MISSION_DATA_COMPLETED,
    STORE_SORTIE,
    STORE_CURRENT_LOCATION,
    ORG_CHANGED,
    ORG_SELECTED,
    ORG_REFRESHED
} from '../Actions/ActionTypes'


/**
 * The document object returned by the API.
 */
export interface CurrentDocumentVersionDto {
    contentType: string,
    creationTime: string,
    description: string,
    documentVersionId: number,
    id: number,
    name: string,
    purpose: string,
    version: number
}

interface MissionList {
    loading: boolean,
    loadingMissions: boolean,
    missionList: any,
    organisationList: any
}

const INITIAL_STATE = {
    loading: false,
    loadingMissions: false,
    missionList: [],
    organisationList: [],
}

export default (state: MissionList = INITIAL_STATE, action): MissionList => {
    switch (action.type) {
        case REFRESH_MISSIONS:
            return {
                ...state,
                loading: true,
            }
        case MISSIONS_CHANGED:
            return {
                ...state,
                loading: false,
                missionList: action.payload.missionList,
                organisationList: action.payload.organisationList
            }
        default:
            return state
    }
};