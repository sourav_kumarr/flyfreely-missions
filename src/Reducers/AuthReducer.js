/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import {
  EMAIL_CHANGED,
  LOGIN_USER,
  LOGIN_USER_FAIL,
  LOGIN_USER_SUCCESS,
  LOGOUT_USER,
  PASSWORD_CHANGED
} from '../Actions/ActionTypes'

const INITIAL_STATE = {
  credentials: __DEV__ ? {email: 'apppilot', password: 'password'} : {email: '', password: ''},
  user: null,
  error: '',
  loading: false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case EMAIL_CHANGED:
      return {
        ...state, 
        credentials: {...state.credentials, email: action.payload},
      }
    case PASSWORD_CHANGED:
      return {
        ...state, 
        credentials: {...state.credentials, password: action.payload},
      }
    case LOGIN_USER:
      return {
        ...state, 
        loading: true, 
        error: '',
      }
    case LOGIN_USER_SUCCESS:
      return {
        ...state, 
        ...INITIAL_STATE, 
        credentials: action.payload.credentials,
        user: action.payload.user,
      }
    case LOGIN_USER_FAIL:
      return {
        ...state, 
        error: 'Authentication Failed.', 
        credentials: {...state.credentials, password: ''},
        loading: false,
      }
    case LOGOUT_USER:
      return {
        ...state, 
        user: null,
        credentials: {
          email: '',
          password: ''
        },      
      }
    default:
      return state
  }
}