/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import { createSelector } from 'reselect'
import { CurrentDocumentVersionDto } from '../Reducers/MissionListReducer';
import { StoredDocument } from '../Reducers/DocumentListReducer';
import { MobileAppState } from '../Reducers';

const getMissionList = (state) => state.MissionListReducer.missionList;

const getMissionResults = (state) => state.MissionDataReducer.missionResults;

const getCurrentMissionId = (state) => state.MissionSelectionReducer;

const getDocumentStatuses = (state: MobileAppState) => state.DocumentListReducer.documentList;

// export const getCurrentMission = createSelector(getMissionList, getCurrentMissionId,
//     (missionList, currentMissionId) => missionList.find(m => m.id === currentMissionId))

// export const getCurrentMissionData = createSelector(getMissionResults, getCurrentMissionId,
//     (missionResults, currentMissionId) => missionResults.find(m => m.id === currentMissionId))

export const getCurrentMission = (state) => {
    const currentMissionId = getCurrentMissionId(state);
    return getMissionList(state).find(m => m.id === currentMissionId)
}

export const getCurrentMissionData = (state) => {
    const currentMissionId = getCurrentMissionId(state);
    return getMissionResults(state).find(m => m.id === currentMissionId)
}

/**
 * Gets the mission documents
 * @param state 
 */
export const getMissionDocuments = (state, missionId: number): Array<CurrentDocumentVersionDto> => 
    getMissionList(state).find(m => m.id === missionId).documents;

export const getMissionDocument = (state, missionId: number, documentId: number): CurrentDocumentVersionDto => 
    getMissionList(state).find(m => m.id === missionId).documents.find((d) => d.id === documentId);

export const getDocumentStatus = (state: MobileAppState, documentId: number): StoredDocument =>
    getDocumentStatuses(state).find(d => d.id === documentId);

export type MissionDocumentStatus = CurrentDocumentVersionDto & StoredDocument;

export const getMissionDocumentStatuses = (state: MobileAppState, missionId: number): Array<MissionDocumentStatus> => {
        const documents = getMissionDocuments(state, missionId);
        return documents.map(d => {
            const status = getDocumentStatus(state, d.id);
            if (status) {
                return {...d, ...status};
            } else {
                return {...d, path: null, downloading: false, progress: 0};
            }
        })
    }