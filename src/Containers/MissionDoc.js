/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import {Text,View,StyleSheet,ListView,Image} from 'react-native';
import Color from '../Components/common/color';
import Header from '../Components/Header';
import {responsiveWidth,responsiveHeight,responsiveFontSize} from 'react-native-responsive-dimensions';

class MissionDoc extends Component {

  constructor (props) {
      super(props);
      const dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.state = {
          dataSource:dataSource
      }
  }

  componentDidMount() {
      // this.setState({dataSource:this.state.dataSource});
      let data = ["Pre-mission Checklist","JSA","Risk Assessment","Post-mission CheckList","Documents"];
      this.setState({dataSource:this.state.dataSource.cloneWithRows(data)});
      console.log('companent did mount is called');
  }

  renderRow(row) {
      return(
          <View style={styles.listItemContainer}>
              <Text style={[styles.textColor,styles.contentFontSize,styles.listText]}>{row}</Text>
              <Image source={require('../assets/imgs/play.png')} style={styles.listImg} resizeMode="contain"/>
              <Image source={require('../assets/imgs/cancel.png')} style={[styles.listImg]} resizeMode="contain"/>

          </View>
      );
  }
  render(){
      console.log('render called');
      return(

          <View style={styles.container}>
              <Header/>
              <View style={styles.headerContainer}>
                  <View style={styles.rightChild}>
                      <Text style={[styles.textBold,styles.textColor,styles.headerFontSize]}>Forest 01</Text>

                  </View>
                  <View style={styles.leftChild}>
                      <Text style={[styles.textBold,styles.textColor,styles.headerFontSize]}>2018-01-22</Text>
                  </View>
              </View>

              <View style={styles.headerContainer}>
                  <View style={styles.rightChild}>
                      <Text style={[styles.textBold,styles.contentFontSize,styles.textColor]}>DGI Phantom 4 Pro</Text>

                  </View>
                  <View style={styles.leftChild}>
                      <Text style={[styles.textBold,styles.contentFontSize,styles.textColor]}>POI</Text>
                  </View>
              </View>

              <View style={styles.headerContainer}>
                  <View style={styles.rightChild}>
                      <Text style={[styles.textBold,styles.contentFontSize,styles.textColor]}>Payload : Built In</Text>

                  </View>
                  <View style={styles.leftChild}>
                      <Text style={[styles.textBold,styles.contentFontSize,styles.textColor]}>RTF</Text>
                  </View>
              </View>
              <View style={styles.divider}/>
              <View style={styles.headerContainer}>
                  <View style={styles.listLeft}>
                     <Text style={[styles.textBold,styles.textColor]}>Completed</Text>
                  </View>
              </View>
              <ListView
               dataSource={this.state.dataSource}
               renderRow={this.renderRow}
              />
         </View>
      );
  }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.defaultBackgroundColor,
        flexDirection:'column'

    },
    headerContainer:{
        flexDirection:'row',
        width:responsiveWidth(100),
        marginTop:3,
        padding:5
    },
    rightChild:{
        width:responsiveWidth(50),
        alignItems:'flex-start',
    },
    leftChild:{
        width:responsiveWidth(50),
        alignItems:'flex-start',
    },
    textBold:{
        fontWeight:'bold',

    },
    textColor:{
      color:'#000'
    },

    headerFontSize:{
        fontSize:responsiveFontSize(3)
    },
    contentFontSize:{
        fontSize:responsiveFontSize(2)
    },
    listFontSize:{
        fontSize:10
    },
    listItemContainer:{
        flexDirection:'row',
        flex:1
    },
    listText:{
      width:responsiveWidth(60)
    },
    listImg:{
      height:responsiveHeight(4),
      width:responsiveWidth(4),
      marginLeft:responsiveWidth(10),
    },
    listLeft:{
      flex:1,
      alignItems:'flex-end',
      paddingRight:responsiveWidth(4)
    },
    divider:{
        borderWidth:1,
        borderColor:Color.borderColor,
        marginTop:responsiveHeight(2)
    }

});
export default MissionDoc;
