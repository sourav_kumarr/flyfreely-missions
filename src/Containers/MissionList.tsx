/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    ListView,
    Picker,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';

import * as actions from '../Actions';
import DataTable from '../Components/datatable/DataTable';
import Style from '../Components/datatable/style';
import Color from '../Components/common/color';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import { formatShortPersonName } from '../Formatters';
import { refreshMissions } from '../Actions';
import { BASE_URL } from '../config';
import { IconButton } from '../Components/common/IconButton';
import { appStyles } from '../Components/common/commonStyles';
import {Button} from "react-native-elements";
import Icon from 'react-native-vector-icons/MaterialIcons'

import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
} from 'react-native-popup-menu';
import { Spinner } from '../Components/common';

interface MissionListProps {
    user: any,
    organisationList: any,
    missionList: any,
    loading: boolean,
    refreshMissions: () => void,
    showMissionDetails: (missionId: number) => void
}

interface MissionListState {
    orgSelected: number,
    orgSelectedIx: number,
    dataSource: any,
    missionID: number,
    sortField: any,
    sortAscending: boolean
}

function compareStr(a, b) {
    const a_ = a.toUpperCase();
    const b_ = b.toUpperCase();
    if (a_ < b_) {
        return 1;
    } else if (a_ > b_) {
        return -1;
    } else {
        return 0;
    }
}

function compare(a, b, field, isAscending: boolean) {
    const invert = isAscending ? 1 : -1;
    if (field == null) {
        return 0;
    } else if (field.label === 'Location') {
        return compareStr(a.location.name, b.location.name) * invert;
    } else if (field.label === 'Org') {
        return compareStr(a.organisationName, b.organisationName) * invert;
    } else if (field.label === 'Name') {
        return compareStr(a.name, b.name) * invert;
    }
}

class MissionList extends React.Component<MissionListProps, MissionListState> {

    fields = [];

    static getDerivedStateFromProps(nextProps: MissionListProps, prevState: MissionListState) {
        if (nextProps.missionList) {
            const filteredData = nextProps.missionList.filter((m) => !prevState.orgSelected || prevState.orgSelected === m.organisationId).map((m) => ({...m}));
            const sortedData = filteredData.sort((a, b) => compare(a, b, prevState.sortField, prevState.sortAscending));

            return {dataSource: prevState.dataSource.cloneWithRows(sortedData)};
        }
    }

    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};

        return {
            headerTitle: 'Missions',
            headerRight: (
                <Menu>
                    <MenuTrigger>
                        <Icon
                         name="more-vert"
                         size={24}
                         color={'grey'}
                        />
                    </MenuTrigger>
                    <MenuOptions customStyles={optionsStyles}>
                        <MenuOption onSelect={() => navigation.navigate('login')} text='Logout' />

                    </MenuOptions>
                </Menu>
            ),
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            dataSource: new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 }),
            orgSelectedIx: 0,
            orgSelected: null,
            missionID: 0,
            sortAscending: true,
            sortField: null
        };

        this.fields = [{
            label: 'Date'
        },
        {
            label: 'Name',
            sortable: true
        },
        {
            label: 'Location',
            sortable: true
        },
        {
            label: 'Org',
            sortable: true
        }];
    }

    componentDidMount() {
        this.props.refreshMissions();
    }

    componentWillMount() {
        this.props.navigation.setParams({ onLogout: this._onLogOut });
    }

    render() {
        const pilotName = formatShortPersonName(this.props.user);
        const organisationPicker = this.renderPicker();
        const missionList = this.renderMissionList();
        const portalUrl = BASE_URL;
        return (
            <View style={styles.container}>
                <View style={[styles.headerContainer, appStyles.row]}>
                    <View style={[styles.border, styles.dropDownOuter]}>
                        {organisationPicker}
                    </View>

                    <Text style={[styles.textBold, styles.headerText]}>{pilotName}</Text>
                    <IconButton icon="refresh" onPress={() => this.props.refreshMissions()} animated={this.props.loading} />
                    {/* <View style={styles.headerImgView}>
                        <Image source={require('../assets/imgs/chat.png')} style={styles.headerImg} />
                    </View> */}
                </View>
                {missionList}
                <Text>To add missions log into the portal at {portalUrl}</Text>
            </View>
        );
    }

    renderPicker() {
        if (this.props.organisationList && this.props.organisationList.length > 0) {
            const serviceItems = this.props.organisationList.map((s, i) => {
                return <Picker.Item key={s.id} value={s.id} label={s.name} />
            });
            // this.setState({dataSource:ds.cloneWithRows(missions)});
            return (
                <Picker
                    mode="dropdown"
                    style={styles.dropDownn}
                    selectedValue={this.state.orgSelected}
                    onValueChange={this.onUpdateOrg1.bind(this)}>
                    <Picker.Item key={0} value={null} label={"All"} />
                    {serviceItems}
                </Picker>
            )
        }
    }

    renderMissionList() {
        if (this.props.loading) {
            return <Spinner color={Color.buttonColor} />;
        } else if (this.props.missionList.length > 0 && this.props.organisationList) {
            return (
                <DataTable
                    dataSource={this.state.dataSource}
                    onSort={this.onSort.bind(this)}
                    fields={this.fields}
                    cellStyle={styles.cellPadding}
                    renderRow={this.renderRow.bind(this)}
                />
            );
        } else {
            return <Text>No missions</Text>;
        }
    }

    _onLogOut() {
     alert('menu Press');
    }

    onPressItem(row) {
        if (row.name) {
            const missionId = row.id;
            this.setState({ missionID: missionId });
            this.props.showMissionDetails(missionId);
        }
    }
    onSort(field, isAscending) {
        this.setState({sortAscending: isAscending, sortField: field}, 
            () => this.setState(MissionList.getDerivedStateFromProps(this.props, this.state)));
    }

    onUpdateOrg1(org, index) {
        if (index !== 0) {
            index = index - 1;
        }
        this.setState({ orgSelectedIx: index, orgSelected: org }, 
            () => this.setState(MissionList.getDerivedStateFromProps(this.props, this.state)));
    }

    renderRow(item) {
        let style = Style.planned;
        if (item.status === 'APPROVED') {
            style = Style.approvedColor;
        }
        else if (item.status === 'READY_TO_FLY') {
            style = Style.rtfColor;
        }

        const missionDate = moment.utc(item.missionDate).local().format("YYYY-MM-DD HH:mm");

        return (
            <TouchableOpacity onPress={() => this.onPressItem(item)}>
                <View style={Style.row}>
                    <Text style={[Style.cell, Style.contentCell, style]}>{missionDate}</Text>
                    <Text style={[Style.cell, Style.contentCell, style]}>{item.name}</Text>
                    <Text style={[Style.cell, Style.contentCell, style]}>{item.location.name}</Text>
                    <Text style={[Style.cell, Style.contentCell, style]}>{item.organisationName}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    onLogout() {
        alert('logout clicked');
       // this.props.navigation.navigate('login');
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Color.defaultBackgroundColor,
        flex: 1,
        width: responsiveWidth(100),
        paddingTop: responsiveWidth(2),
        flexDirection: 'column'
    },
    dataTableContainer: {
        padding: 2
    },
    dropDownn: {
        height: responsiveHeight(5),
        width: responsiveWidth(45),

    },
    dropDownOuter: {
        marginLeft: responsiveWidth(2)
    },
    headerContainer: {
        flexDirection: 'row',
        width: responsiveWidth(100)
    },
    border: {
        borderWidth: 1,
        borderColor: Color.defaultTextColor
    },
    textBold: {
        fontWeight: 'bold'
    },
    headerText: {
        marginTop: responsiveHeight(0),
        marginLeft: responsiveWidth(2),
        color: Color.defaultTextColor,
        fontSize: responsiveFontSize(3)
    },
    headerImgView: {
        flex: 1,
        alignItems: 'flex-end',
        marginRight: responsiveWidth(2)
    },
    headerImg: {
        width: responsiveWidth(10),
        height: responsiveHeight(5),
    }

});

const optionsStyles = StyleSheet.create({
    optionText: {
        padding: 5
    }

});


const mapStateToProps = state => {
    return {
        missionList: state.MissionListReducer.missionList,
        organisationList: state.MissionListReducer.organisationList,
        user: state.AuthReducer.user,
        loading: state.MissionListReducer.loading
    }
}

export default connect(mapStateToProps, actions)(MissionList)

