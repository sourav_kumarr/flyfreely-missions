/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    Text,
    View,
    FlatList,
    Keyboard,
    PermissionsAndroid,
    Platform,
    Alert,
    BackHandler,
    DeviceEventEmitter
} from 'react-native';
import { connect } from 'react-redux';

import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import MapView, { Polygon, } from 'react-native-maps';
import FusedLocation from 'react-native-fused-location';
import NfcManager from 'react-native-nfc-manager';
import moment from 'moment';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

import Header from '../Components/Header';
import * as actions from '../Actions'
import inside from '../utils/pointInPolygon';
import Color from '../Components/common/color';
import {
    screenWidth,
    screenHeight,
} from '../Components/common/commonStyles';

import {
    Card,
    CardSection,
    ButtonSection,
    Button
} from '../Components/common';
import MissionOverview from '../Components/Mission/MissionOverview'

import { getCurrentMission, getCurrentMissionData } from '../Selectors';
import DocumentationView from '../Components/Mission/DocumentationView';
import MissionView from '../Components/Mission/MissionView';

import {
    MissionState, 
    ProgressStep, 
    MissionResults, 
    SORTIE_NEW,
    SORTIE_STARTED,
    SORTIE_FLYING,
    SORTIE_ABORTED,
    SORTIE_COMPLETED,
    MISSION_CANCELLED,
    MISSION_COMPLETED,
    MISSION_FLYING
} from '../Domain';

enum DisplayState {
    MISSION,
    DOCUMENTATION
}

//steps
const NONE = -1;

interface MissionDetailsProps {
    storeMission: (missionId: number, status: string, step: string, checklists: any, forms: any) => void,
    storeSortie: (missionId: number, sortie: any, callback?: any) => void,
    storeCurrentLocation: (location: any, callback?: any) => void,
    showCheckList: (step: string, checklists: any, callback: any) => void,
    mission: any,
    missionResults: MissionResults
}

interface MissionDetailsState {
    showing: DisplayState,
    nfcTagId: any,
    missionState: MissionState,
}

class MissionDetails extends Component<MissionDetailsProps, MissionDetailsState> {
    currentChecklist: any // list of checklists for the current step
    subscription: any // location callback 
    watchId: any // ??
    checklists: any
    mission: any
    location: any
    craft: any
    startTime: any
    endTime: any
    currentSortie: any

    static getDerivedStateFromProps(nextProps: MissionDetailsProps, prevState: MissionDetailsState) {
        return null;
    }

    constructor(props) {
        super(props);

        this.state = {
            showing: DisplayState.MISSION,
            missionState: {
                isLocationVerified: false,
                isNfcSupported: false,
                isNfcVerified: false,
                isVerificationOverridden: false,
                progressStep: ProgressStep.STEP_BEFORE_MISSION_START,
                location: { latitude: 0, longitude: 0 },
            },
            nfcTagId: '',
        };

        this.currentSortie = {
            number: 0,
            status: SORTIE_NEW,
            startTime: '',
            endTime: '',
            checklistResponses: {},
        };

        this.currentChecklist = [];
        this.startTime = null;
        this.endTime = null;
    }

    componentDidMount() {

        Keyboard.dismiss();

        if (Platform.OS === 'ios') {
            this.getiOSLocation();
        } else {
            this.getAndroidLocation();
        }

        NfcManager.isSupported()
            .then(supported => {
                this.setState((state) => ({ ...state, missionState: { ...state.missionState, isNfcSupported: supported } }));
                if (supported) {
                    this.startNfcDetection();
                }
            })
    }

    componentWillUnmount() {
        if (Platform.OS === 'ios') {
            navigator.geolocation.clearWatch(this.watchId);
        } else {
            if (this.subscription) {
                FusedLocation.off(this.subscription);
                FusedLocation.stopLocationUpdates();
            }
        }
        this.stopNfcDetection();
        // LocationServicesDialogBox.stopListener();
    }


    getiOSLocation() {
        this.watchId = navigator.geolocation.watchPosition(
            (position) => this.storeCurrentLocation(position.coords.latitude, position.coords.longitude),
            (error) => {
                console.log('Location Error : ', error);
            },
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },// , distanceFilter: 10
        );
    }

    storeCurrentLocation(latitude: number, longitude: number) {
        this.props.storeCurrentLocation({
            latitude,
            longitude
        });
        this.setState((state) => ({
            ...state,
            missionState: {
                ...state.missionState, isLocationVerified: this.checkMyLocation(),
                location: {
                    latitude,
                    longitude
                },
            }
        }));
    }


    async getAndroidLocation() {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
                title: 'App needs to access your location',
                message: 'App needs access to your location ' +
                    'to verify your flight area.'
            }
        );

        if (granted) {
            FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY);

            // Get location once.
            try {
                const location = await FusedLocation.getFusedLocation();

                this.storeCurrentLocation(
                    location.latitude,
                    location.longitude,
                );
            } catch (e) {
                return;
            }

            // Set options.
            FusedLocation.setLocationPriority(FusedLocation.Constants.BALANCED);
            FusedLocation.setLocationInterval(20000);
            FusedLocation.setFastestLocationInterval(15000);
            FusedLocation.setSmallestDisplacement(10);

            // Keep getting updated location.
            FusedLocation.startLocationUpdates();

            // Place listeners.
            this.subscription = FusedLocation.on('fusedLocation', location => {
                this.storeCurrentLocation(location.latitude, location.longitude);
            });
        }
    }


    checkMyLocation() {
        if (this.props.mission.location.coordinates.length == 0) {
            return true;
        }

        let coordinates = [];
        this.props.mission.location.coordinates.map((entry) => {
            let item = [entry.latitude, entry.longitude];
            coordinates.push(item);
        });

        const inPolygon = inside([this.state.missionState.location.latitude, this.state.missionState.location.longitude], coordinates);
        if (!inPolygon) {
            return false;
        }

        return true;
    }


    checkNfc() {
        if (this.props.mission.craft.nfcUid === null) {
            this.setState((state) => ({ ...state, missionState: { ...state.missionState, isNfcVerified: false } }));
            return true;
        }

        if (this.props.mission.craft.nfcUid.toLowerCase() === this.state.nfcTagId.toLowerCase()) {
            this.setState((state) => ({ ...state, missionState: { ...state.missionState, isNfcVerified: true } }));
            return true;
        }

        this.setState((state) => ({ ...state, missionState: { ...state.missionState, isNfcVerified: false } }));
        return false;
    }


    //nfc functions
    startNfcDetection() {
        NfcManager.registerTagEvent(this.onNfcTagDiscovered.bind(this))
            .then(result => {
                console.log('NFC registerTagEvent Success : ', result)
            })
            .catch(error => {
                console.warn('NFC registerTagEvent Fail : ', error)
            });
    }


    stopNfcDetection() {
        NfcManager.unregisterTagEvent()
            .then(result => {
                console.log('NFC unregisterTagEvent Success : ', result)
            })
            .catch(error => {
                console.warn('NFC unregisterTagEvent Fail : ', error)
            });
    }


    onNfcTagDiscovered(tag) {
        this.setState({
            nfcTagId: tag.id,
        }, () => {
            this.checkNfc();
        });
    }

    onToggleDocumentation() {
        this.setState((prevState, props) => ({ ...prevState, showing: prevState.showing == DisplayState.DOCUMENTATION ? DisplayState.MISSION : DisplayState.DOCUMENTATION }))
    }

    /**
     * Check if there is documentation required.
     * 
     * @param step the mission step that we are checking
     */
    checkRequiredDocumentation(step: string) {
        if (this.props.mission.checklists[step].length > 0) {
            this.currentChecklist = this.props.mission.checklists[step];
            return true;
        }

        return false;
    }

    onStartSortie() {
        const isLocationCorrect = this.checkMyLocation();
        const isCraftCorrect = this.checkNfc();

        if (!isLocationCorrect || !isCraftCorrect) {
            let message;
            if (!isLocationCorrect && !isCraftCorrect) {
                message = "Location and craft not verified"
            } else if (!isLocationCorrect) {
                message = "Location not verified";
            } else if (!isCraftCorrect) {
                message = "Craft not verified";
            }

            Alert.alert(message, "Continue anyway?",
                [
                    { text: 'Yes', onPress: () => this.preSortie() },
                    { text: 'No', style: 'cancel' }
                ])
        } else {
            this.preSortie();
        }
    }

    preSortie() {
        if (!this.checkRequiredDocumentation('Sortie.pre-flight')) {
            this.startSortie([]);
            return;
        }

        this.props.showCheckList('Pre-flight', this.currentChecklist, (checklistResponses) => this.startSortie(checklistResponses));
    }

    startSortie(checklistResponses) {
        this.startTime = moment().utc();
        this.currentSortie = {
            number: this.currentSortie.number + 1,
            status: SORTIE_FLYING,
            startTime: this.startTime.format(),
            endTime: '',
            // duration: 0,
            checklistResponses: {},
        };

        this.currentSortie.checklistResponses['pre-flight'] = checklistResponses;

        this.setState((state) => {
            return {
                ...state, missionState: {
                    ...state.missionState, progressStep: ProgressStep.STEP_SORTIE_STARTED,
                }
            };
        })
    }

    onEndSortie() {
        this.endTime = moment().utc();
        // const duration = this.endTime.diff(this.startTime);

        this.currentSortie = {
            ...this.currentSortie,
            status: SORTIE_COMPLETED,
            endTime: this.endTime.format(),
        }

        if (!this.checkRequiredDocumentation('Sortie.post-flight')) {
            this.storeSortie([]);
            return;
        }

        this.props.showCheckList('Post-flight', this.currentChecklist, (checklistResponses) => this.storeSortie(checklistResponses));
    }


    onAbortSortie() {
        this.currentSortie = {
            ...this.currentSortie,
            status: SORTIE_ABORTED,
            endTime: moment().utc().format(),
        }

        if (!this.checkRequiredDocumentation('Sortie.post-flight')) {
            this.storeSortie([]);
            return;
        }

        this.props.showCheckList('Post-flight', this.currentChecklist, (checklistResponses) => this.storeSortie(checklistResponses));
    }


    storeSortie(checklistResponses) {
        this.currentSortie.checklistResponses['post-flight'] = checklistResponses;

        this.setState((state) => {
            return {
                ...state, missionState: {
                    ...state.missionState, progressStep: ProgressStep.STEP_MISSION_STARTED,
                }
            };
        })
        this.props.storeSortie(this.props.mission.id, this.currentSortie);
    }

    startMission(checklistResponses) {
        this.props.storeMission(this.props.mission.id, MISSION_FLYING, 'pre-mission', checklistResponses, []);
        this.setState((state) => ({
            ...state, missionState: {
                ...state.missionState,
                progressStep: ProgressStep.STEP_MISSION_STARTED
            }
        }));
    }

    onStartMission() {
        if (!this.checkRequiredDocumentation('Mission.pre-mission')) {
            this.startMission([]);
        }

        this.props.showCheckList('Pre-mission', this.currentChecklist, (checklistResonses) => this.startMission(checklistResonses));
    }

    onEndMission() {
        if (!this.checkRequiredDocumentation('Mission.post-mission')) {
            this.storeMissionData(MISSION_COMPLETED, []);
            return;
        }

        this.props.showCheckList('Post-mission', this.currentChecklist, (checklistResonses) => this.storeMissionData(MISSION_COMPLETED, checklistResonses));
    }

    //end mission
    storeMissionData(status, checklistResonses) {
        // if (this.state.sorties.length > 0) {
        //     this.props.postMissionData(this.state.missionID, 'COMPLETED');
        // }

        // const { state, goBack } = this.props.navigation
        // const params = state.params || {}
        // goBack(params.go_back_key)
        // return;
        this.props.storeMission(this.props.mission.id, status, 'post-mission', checklistResonses, []);
    }

    onAbortMission() {
        if (!this.checkRequiredDocumentation('Mission.post-mission')) {
            this.storeMissionData(MISSION_CANCELLED, []);
            return;
        }

        this.props.showCheckList('Post-mission', this.currentChecklist, (checklistResonses) => this.storeMissionData(MISSION_CANCELLED, checklistResonses));
    }

    render() {
        const isShowingDocs = this.state.showing == DisplayState.DOCUMENTATION;
        const currentView = isShowingDocs ? <DocumentationView mission={this.props.mission} missionResults={this.props.missionResults} /> :
            <MissionView
            mission={this.props.mission}
            missionState={this.state.missionState}
            missionResults={this.props.missionResults}
            onStartMission={this.onStartMission.bind(this)}
            onAbortMission={this.onAbortMission.bind(this)}
            onEndMission={this.onEndMission.bind(this)}
            onStartSortie={this.onStartSortie.bind(this)}
            onEndSortie={this.onEndSortie.bind(this)}
            onAbortSortie={this.onAbortSortie.bind(this)} />

        return (

            <View style={styles.container}>
                <MissionOverview
                    mission={this.props.mission}
                    missionState={this.state.missionState}
                    isDocsOpen={isShowingDocs}
                    onToggleDocumentation={this.onToggleDocumentation.bind(this)}
                />
                <ScrollView
                    style={styles.scrollView}
                    contentContainerStyle={styles.scrollViewContent}
                    showsVerticalScrollIndicator={false}
                >
                    {currentView}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.defaultBackgroundColor,
        padding: 5

    },
    scrollView: {
        width: screenWidth - 10,
    },
    scrollViewContent: {
        alignItems: 'flex-start',
    },
    textFiledName: {
        fontSize: 16,
        color: Color.defaultTextColor,
        fontWeight: 'bold',
        // width: 90,
    },
    textValue: {
        flex: 1,
        fontSize: 16,
        color: Color.defaultTextColor,
    },
    textBold: {
        fontWeight: 'bold',
    },
    sortierDetailContainer: {
        width: screenWidth - 20,
        borderWidth: 1,
        borderColor: Color.borderColor,
    },
    objectiveContainer: {
        flex: 1,
        alignItems: 'flex-end',
    },
    buttonContainer: {
        flexDirection: 'row',
        width: responsiveWidth(95),
        marginTop: 3
    },
    objectiveContainer1: {
        width: responsiveWidth(40),
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
});


const mapStateToProps = state => {
    return {
        mission: getCurrentMission(state),
        missionResults: getCurrentMissionData(state),
        missionList: state.MissionListReducer.missionList,

    }
};

export default connect(mapStateToProps, actions)(MissionDetails)