/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Alert,
    ImageBackground,
    TouchableOpacity,
    Linking,
    TextInputProperties

} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../Actions'
import {
    CardSection,
    Spinner,
} from '../Components/common';
import Color from '../Components/common/color';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import ResponsiveImage from 'react-native-responsive-image'
import { BASE_URL } from '../config';
import { appStyles } from '../Components/common/commonStyles';

const logoImage = require('../assets/imgs/header.png');
const thumbImage = require('../assets/imgs/fingerprint.png');

interface LoginFormProps {
    email: string,
    password: string,
    error: string,
    loading: boolean,
    emailChanged: (email: string) => void,
    passwordChanged: (password: string) => void,
    loginUser: (email: string, password: string) => void
}


class LoginForm extends Component<LoginFormProps> {
    email: TextInput;
    password: TextInput;

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.email.focus();
    }

    onChangeEmail(text) {
        this.props.emailChanged(text);
    }

    onChangePassword(text) {
        this.props.passwordChanged(text);
    }

    onLogin() {
        const { email, password } = this.props

        if (email === '' || password === '') {
            Alert.alert("Please enter credential");
            return;
        }

        this.props.loginUser(email, password);
    }

    renderLoginButton() {
        if (this.props.loading) {
            return (
                <CardSection style={{ marginVertical: 20 }}>
                    <Spinner color={Color.buttonColor} />
                </CardSection>
            );
        }
        return (
            <CardSection>
                <TouchableOpacity
                    onPress={this.onLogin.bind(this)}>
                    <Image
                        source={require('./../assets/imgs/login.png')}
                        style={styles.loginBtn}
                        resizeMode="contain"
                    />
                </TouchableOpacity>


            </CardSection>
        );
    }

    openForgotPassword() {
        Linking.openURL(BASE_URL + '/#!/resetpassword');
    }

    openSignup() {
        Linking.openURL(BASE_URL + '/#!/signup');
    }

    render() {
        return (
            <ImageBackground
                source={require('./../assets/imgs/planemobilescreen.jpg')}
                style={styles.backgroundImage}>
                <View style={styles.container}>
                    <View style={styles.topContainer}>
                        <Image source={logoImage} style={styles.imageLogo} resizeMode="contain" />
                        {/* <Text style={[styles.textTitle, styles.textBold]}>FlyFreely</Text>*/}

                    </View>
                    <Text style={styles.previewRelease}>Preview Release</Text>
                    {/* <Text style={[styles.textMargin, styles.textBold]}> Missions</Text> */}
                    <CardSection style={{ marginTop: responsiveHeight(3) }}>
                        {/*<Text style={styles.textFieldName}>Username / email :</Text>*/}
                        <TextInput
                            ref={ref => (this.email = ref)}
                            autoCorrect={false}
                            autoCapitalize="none"
                            underlineColorAndroid="transparent"
                            placeholder="Username"
                            keyboardType="email-address"
                            returnKeyType="next"
                            editable={!this.props.loading}
                            style={[styles.inputText, styles.inputUserNameWidth]}
                            value={this.props.email}
                            onChangeText={this.onChangeEmail.bind(this)}
                            onSubmitEditing={() => this.password.focus()}
                        />
                    </CardSection>

                    <CardSection>
                        {/*<Text style={styles.textFieldName}>Password :</Text>*/}
                        <TextInput
                            ref={ref => (this.password = ref)}
                            autoCorrect={false}
                            autoCapitalize="none"
                            underlineColorAndroid="transparent"
                            secureTextEntry
                            placeholder="password"
                            editable={!this.props.loading}
                            style={[styles.inputText, styles.inputUserPassWidth]}
                            value={this.props.password}
                            returnKeyType="go"
                            onChangeText={this.onChangePassword.bind(this)}
                            onSubmitEditing={() => this.onLogin()}
                        />
                    </CardSection>

                    <Text style={styles.textAuthError}>{this.props.error}</Text>

                    {this.renderLoginButton()}
                    <Text style={styles.forgotPassword} onPress={this.openForgotPassword.bind(this)}>
                        Forgot Password ?
        </Text>

                    <Text style={[styles.textBold, styles.signUp]} onPress={this.openSignup.bind(this)}>
                        Sign Up
        </Text>
                    {/*<View style={styles.touchOutside}>
         <View style={styles.touchContainer}>
           <Text style={{fontWeight:'bold'}}>or Login using Touch ID</Text>
           <Image source={thumbImage} style={styles.thumbLogo} resizeMode="contain" />
         </View>
        </View>*/}
                </View>
            </ImageBackground>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        paddingHorizontal: 8,
        paddingVertical: 10,
        height: responsiveHeight(100)
    },
    topContainer: {
        flexDirection: 'row',
        marginHorizontal: 8
    },
    imageLogo: {
        width: responsiveHeight(30),
        height: responsiveHeight(17),
    },
    thumbLogo: {
        width: 70,
        height: 70,
        marginTop: responsiveHeight(2)
    },
    textTitle: {
        color: Color.defaultTextColor,
        fontSize: 25,
        marginLeft: 10
    },
    textMargin: {
        marginTop: 5,
        fontSize: 32,
        color: Color.defaultTextColor,
    },
    textBold: {
        fontWeight: 'bold',
    },
    textFieldName: {
        flex: 2,
        fontSize: 14,
        textAlign: 'right',
        fontWeight: 'bold',
    },
    inputText: {
        color: Color.defaultTextColor,
        fontSize: 14,
        height: 30,
        paddingHorizontal: responsiveHeight(2),
        paddingVertical: 2,
        marginLeft: 10,
        borderTopColor: Color.borderColor,
        borderRightWidth: responsiveWidth(7),
        borderTopWidth: responsiveWidth(12),
        borderRightColor: 'transparent',
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10
    },
    inputUserNameWidth: {
        width: responsiveWidth(70)
    },
    inputUserPassWidth: {
        width: responsiveWidth(60)
    },
    textAuthError: {
        fontSize: 20,
        alignSelf: 'center',
        color: Color.errorTextColor,
    },
    loginBtn: {
        width: responsiveWidth(50),
        height: responsiveHeight(10),
        marginLeft: responsiveWidth(2)
    },
    touchContainer: {
        padding: responsiveHeight(2),
        width: responsiveWidth(60),
        borderWidth: 2,
        borderColor: Color.defaultTextColor,
        borderRadius: 20,
        backgroundColor: Color.touchBack,
        marginTop: responsiveHeight(5),
        justifyContent: 'center',
        alignItems: 'center'

    },
    touchOutside: {
        flexDirection: 'row',
        width: responsiveWidth(100),
        justifyContent: 'center',
        alignItems: 'center'

    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
    },
    forgotPassword: {
        marginLeft: responsiveWidth(2),
        textDecorationLine: "underline",
        textDecorationStyle: "solid",
        textDecorationColor: "#000",
        fontSize: responsiveFontSize(2)
    },
    signUp: {
        marginLeft: responsiveWidth(2),
        marginTop: responsiveHeight(5),
        fontSize: responsiveFontSize(4)
    },
    previewRelease: {
        fontStyle: 'italic',
        marginHorizontal: 8
    }

});

const mapStateToProps = ({ AuthReducer }) => {
    const { credentials, error, loading } = AuthReducer
    const { email, password } = credentials;
    return { email, password, error, loading }
}

export default connect(mapStateToProps, actions)(LoginForm)