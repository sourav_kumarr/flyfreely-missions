/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    SectionList,
    Alert,
} from 'react-native';
import { connect } from 'react-redux';
import CheckBox from 'react-native-check-box'

import { refreshMissions } from '../Actions';
import Color from '../Components/common/color';
import {
    Button,
    ButtonSection,
} from '../Components/common';

function capitalizeFirstLetter(string) {
    if (!string) {
        return;
    }
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}
let ref = null;

interface ChecklistProps {
    currentCheckList: any,
    checkListName: string,
    onChecklistCompleted: any,
    navigation: any
}


class CheckList extends Component<ChecklistProps> {
    static navigationOptions = ({ navigation }) => {
        const { state, setParams } = navigation;
        return {
            title: state.params && capitalizeFirstLetter(state.params.checkListName) + ' checks',
        };
    }

    checkedList: any;

    constructor(props) {
        super(props)

        this.state = {
        };

        this.checkedList = [];
        // this.onClose = this.onClose.bind(this);
    }



    componentDidMount() {
        this.props.navigation.setParams({
            checkListName: this.props.checkListName,
        });

        //initialize
        this.props.currentCheckList.map((entry, index) => {
            this.checkedList.push([]);
        });
    }


    renderSectionHeader(section) {
        return (
            <View style={styles.sectionHeaderContainer}>
                <Text style={styles.textSectionHeaderTitle}>{section.key}</Text>
            </View>
        );
    }


    onChecked(checkListIndex, item) {
        const id = this.checkedList[checkListIndex].find(id => id === item.id);
        if (!id) {
            this.checkedList[checkListIndex].push(item.id);
        } else {
            const index = this.checkedList[checkListIndex].indexOf(item.id);
            if (index > -1) {
                this.checkedList[checkListIndex].splice(index, 1);
            }
        }
    }


    renderItem(index, item) {
        return (
            <CheckBox
                style={styles.checkBox}
                onClick={() => this.onChecked(index, item)}
                rightText={item.label}
            />
        );
    }


    onOk() {
        let isRequired = false;
        let description = '';
        for (let i = 0; i < this.props.currentCheckList.length; i++) {
            const entry = this.props.currentCheckList[i];
            for (let j = 0; j < entry.sections.length; j++) {
                const section = entry.sections[j];
                for (let k = 0; k < section.questions.length; k++) {
                    const question = section.questions[k];
                    const id = this.checkedList[i].find(id => id === question.id);
                    if (!id && question.required) {
                        isRequired = true;
                        description = question.label;
                        break;
                    }
                }

                if (isRequired) {
                    break;
                }
            }

            if (isRequired) {
                break;
            }
        };

        if (isRequired) {
            Alert.alert('Checklist', 'You should check for ' + '"' + description + '"');
            return
        }
        this.props.onChecklistCompleted(this.buildResponses());
        this.props.navigation.goBack(null);
    }

    buildResponses() {
        return this.checkedList.map((responses, ix) => ({
            checklistVersionId: this.props.currentCheckList[ix].id,
            values: responses
        }));
    }

    onClose() {
        this.props.navigation.goBack(null);
    }

    render() {
        return (
            <View style={styles.container}>
                {
                    this.props.currentCheckList.map((entry, index) => {

                        let sections = [];

                        entry.sections.map((subEntry) => {
                            let subSection = {};
                            subSection['key'] = subEntry.name;
                            subSection['data'] = subEntry.questions;

                            sections.push(subSection);
                        });

                        return (
                            <View key={index} style={styles.container}>
                                <View style={styles.topContainer}>
                                    {
                                        entry.checklistName &&
                                        <Text style={styles.textName}>{entry.checklistName}</Text>
                                    }
                                    {
                                        entry.description &&
                                        <Text style={styles.textDescription}>{entry.description}</Text>
                                    }
                                </View>
                                <SectionList
                                    style={styles.sectionContainer}
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={(item, index) => index}
                                    renderSectionHeader={({ section }) => this.renderSectionHeader(section)}
                                    renderItem={({ item }) => this.renderItem(index, item)}
                                    sections={sections}
                                />

                                <ButtonSection>
                                    <Button
                                        style={styles.button}
                                        title='Ok'
                                        onPress={this.onOk.bind(this)}
                                    />
                                    <Button
                                        style={styles.button}
                                        title='Cancel'
                                        onPress={this.onClose.bind(this)}
                                    />
                                </ButtonSection>
                            </View>
                        );
                    })
                }
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.defaultBackgroundColor,
    },
    topContainer: {
        marginHorizontal: 10,
        marginVertical: 10,
    },
    textName: {
        color: Color.defaultTextColor,
        fontSize: 16,
        fontWeight: 'bold',
        backgroundColor: 'transparent',
        marginVertical: 5,
    },
    textDescription: {
    },
    sectionContainer: {
        marginHorizontal: 10,
    },
    sectionHeaderContainer: {
        marginTop: 30,
        marginBottom: 5,
    },
    textSectionHeaderTitle: {
        color: Color.defaultTextColor,
        fontSize: 22,
        fontWeight: 'bold',
        backgroundColor: 'transparent',
    },
    button: {
        marginHorizontal: 10,
    },
    checkBox: {
        flex: 1,
        padding: 5
    },

});


const mapStateToProps = state => {
    return {
        checkListName: state.CheckListReducer.checkListName,
        currentCheckList: state.CheckListReducer.selctedCheckList,
        onChecklistCompleted: state.CheckListReducer.onChecklistCompleted,
    }
}

export default connect(mapStateToProps)(CheckList)
