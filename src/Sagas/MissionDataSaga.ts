/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'
import {
  missionsUpdateFailed,
  updateMissions,
  postMissionData,
  postMissionDataFailed,
  postMissionDataCompleted,
  updateOrganisations,
  orgUpdateFailed,
  refreshMissions
} from '../Actions'
import {
  POST_MISSION_DATA,
  REFRESH_MISSIONS,
  STORE_MISSION
} from '../Actions/ActionTypes'
import axios from 'axios'
import { BASE_URL } from '../config';
import { MISSION_CANCELLED, MISSION_COMPLETED } from '../Domain';



function* doUpdateMission(action) {
  try {
    const missionList = yield select((state) => state.MissionDataReducer.missionResults)
    const mission = missionList.find(m => m.id === action.payload.missionId)

    if (!mission.dirty) {
      return;
    }
    // Save to the server
    const finaliseMissionCommand = {
      status: mission.status,
      sorties: mission.sorties,
      formResponses: mission.formResponses,
      checklistResponses: mission.checklistResponses,
      message: ''
    }

    console.log('Update Mission : ', finaliseMissionCommand);

    const response = yield call(writeMissionData, action.payload.missionId, finaliseMissionCommand)
    yield put(postMissionDataCompleted(response))
    yield put(refreshMissions())
  } catch (e) {
    yield put(postMissionDataFailed(e.toString()))
  }
}

function writeMissionData(missionId, updateMissionStatusCommand) {
  return axios.put(BASE_URL + '/mobileapi/user/missions/' + missionId + '/status', updateMissionStatusCommand)
    .then((response) => {
      return response;
    })
}

function* doTryPost(action) {
  const missionList = yield select((state) => state.MissionDataReducer.missionResults)
  const mission = missionList.find(m => m.id === action.payload.missionId)

  if (mission.status != MISSION_CANCELLED && mission.status != MISSION_COMPLETED) {
    return;
  }

  yield put(postMissionData(action.payload.missionId));
}

export default function* root() {
  yield takeEvery(POST_MISSION_DATA, doUpdateMission)
  yield takeEvery(STORE_MISSION, doTryPost)
}