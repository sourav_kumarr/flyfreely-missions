/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import { put, takeEvery, call, select, all, take } from 'redux-saga/effects'
import {
    DocumentListActions
} from '../Actions/ActionTypes'
import { NavigationActions } from 'react-navigation'
import RNFetchBlob from 'react-native-fetch-blob'
import FileOpener from 'react-native-file-opener'
import { Buffer } from 'buffer'
import { BASE_URL } from '../config';
import {
    updateDocuments,
    downloadFileError,
    downloadFileFinished,
    downloadFileProgress
} from '../Actions'
import { END, eventChannel } from "redux-saga";
import { StoredDocument } from '../Reducers/DocumentListReducer';
import { DownloadSingleFileAction, ViewDocumentAction } from '../Actions/DocumentListActionCreators';
import { getMissionDocuments, getDocumentStatus, getMissionDocument } from '../Selectors';
import { CurrentDocumentVersionDto } from '../Reducers/MissionListReducer';
import { MobileAppState } from '../Reducers';

function buildDocumentUrl(missionId, documentId) {
    return BASE_URL + '/mobileapi/user/missions/' + missionId + '/documents/' + documentId;
}

function* doShowMissionDocumentation() {
    yield put(NavigationActions.navigate({ routeName: 'missionDocumentation' }))
}

function* doDownloadAndShowDocument(action: ViewDocumentAction) {
    const document: CurrentDocumentVersionDto =
            yield select((state: MobileAppState) => getMissionDocument(state, action.payload.missionId, action.payload.documentId));
    const documentStatus = yield select((state: MobileAppState) => getDocumentStatus(state, action.payload.documentId));

    if (!document) {
        alert("Can't find document");
        return;
    }
    console.log(document);
    const isExists = yield call(() => RNFetchBlob.fs.exists(documentStatus.path));
    console.log('is file xexists ' + isExists + "\npath " + documentStatus.path);
    if (isExists) {
        FileOpener.open(documentStatus.path, document.contentType);
    }
    else {
        alert("File is not downloaded yet");
    }

}

function downloadFileSagaHelper(_id, url, filePath, authString) {
    // An event channel will let you send an infinite number of events
    // It provides you with an emitter to send these events
    // These events can then be picked up by a saga through a "take" method
    console.log('url ' + url + "\n file path " + filePath + "\n authstring " + authString);

    return eventChannel(emitter => {
        RNFetchBlob.config({
            fileCache: true,
            path: filePath
        }).fetch('GET', url, { Authorization: authString })
            .progress({ interval: 200 }, (received, total) => {
                const progress = received / total * 100;
                // const progress = 0;
                console.log('received ' + received + ' total ' + total + "\n");
                // I chose to emit actions immediately
                emitter(downloadFileProgress(_id, progress));
            })
            .then(res => {
                // downloadFileFinished(_id,res.path());
                emitter(
                    downloadFileFinished(_id, res.path()));
                // emitter(END);
                // END event has to be sent to signal that we are done with this channel

            }).
            catch((errorMessage) => {
                // emitter(END);
                emitter(downloadFileError(_id, errorMessage));
            });

        // The returned method can be called to cancel the channel
        return () => {
            emitter(END);
        };
    });
}

function* doDownload(action: DownloadSingleFileAction) {
    const document: CurrentDocumentVersionDto =
        yield select((state: MobileAppState) => getMissionDocument(state, action.payload.missionId, action.payload.documentId));

    const dirs = RNFetchBlob.fs.dirs;
    const fileExt = document.contentType.split("/")[1];
    const _id = action.payload.documentId;
    const filePath = dirs.DocumentDir + "/" + action.payload.documentId + '.' + fileExt;
    const url = buildDocumentUrl(action.payload.missionId, action.payload.documentId) + "." + fileExt;
    const auth = yield select((state) => state.AuthReducer.credentials)
    const authString = Buffer.from(auth.username + ':' + auth.password).toString('base64');

    const channel = yield call(downloadFileSagaHelper, _id, url, filePath, authString);
    // const channel = yield call(countdown, _id, 10);

    try {
        // take(END) will cause the saga to terminate by jumping to the finally block
        while (true) {
            // Remember, our helper only emits actions
            // Thus we can directly "put" them
            const action = yield take(channel);
            yield put(action);
        }
    } catch (error) {
        channel.close();
        // put(downloadFileError(error,_id));
    }
}

function* iterate(item) {
    let dirs = RNFetchBlob.fs.dirs;
    let fileExt = item.contentType.split("/")[1];
    let path = dirs.DocumentDir + "/" + item.id + '.' + fileExt;
    item.isDownloaded = yield call(() => RNFetchBlob.fs.exists(path));
    if (item.isDownloaded) {
        item.path = path;

    }
}

function* doFilterDocuments(action) {
    let documents = action.payload.docList;
    yield documents.map(item => call(iterate, item));

    const payload = { documents };
    yield put(updateDocuments(payload));
}

export default function* root() {
    yield takeEvery(DocumentListActions.SHOW_MISSION_DOCUMENTATION, doShowMissionDocumentation)
    yield takeEvery(DocumentListActions.VIEW_DOCUMENT, doDownloadAndShowDocument)
    yield takeEvery(DocumentListActions.FILTER_DOCUMENT_LIST, doFilterDocuments)
    yield takeEvery(DocumentListActions.DOWNLOAD_SINGLE_FILE, doDownload)

}