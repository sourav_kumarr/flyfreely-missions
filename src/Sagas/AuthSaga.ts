/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/

import { take, call, put, fork, race, takeEvery, takeLatest } from 'redux-saga/effects'
import { NavigationActions } from 'react-navigation'
import {
    loginUserFail,
    loginUserSuccess,
} from '../Actions'
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
} from '../Actions/ActionTypes'
import axios from 'axios'
import { BASE_URL } from '../config';


function* loginFlow() {
    while (true) {
        const loginRequest = yield take(LOGIN_USER);

        const auth = {
            username: loginRequest.payload.email,
            password: loginRequest.payload.password
        }

        try {
            const response = yield call(axios.get, BASE_URL + '/mobileapi/user', { auth: auth })
            yield put(loginUserSuccess(auth, response.data));
        } catch (e) {
            yield put(loginUserFail());
        }
    }
}

/**
 * On a successful login show the missions screen.
 * 
 */
function* postLogin() {
    yield put(NavigationActions.navigate({ routeName: 'missions' }));
}

export default function* root() {
    yield fork(loginFlow);
    yield takeLatest(LOGIN_USER_SUCCESS, postLogin);
}