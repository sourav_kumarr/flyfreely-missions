/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import { fork } from 'redux-saga/effects'
import authSaga from './AuthSaga'
import missionSaga from './MissionListSaga'
import missionDetailSaga from './MissionDetailSaga'
import checkListSaga from './CheckListSaga'
import missionDocumentationSaga from './DocumentListSaga'
import missionDataSaga from './MissionDataSaga'

// Just reexport the only saga
export default function* root() {
    yield fork(authSaga)
    yield fork(missionSaga)
    yield fork(missionDetailSaga)
    yield fork(checkListSaga)
    yield fork(missionDocumentationSaga)
    yield fork(missionDataSaga)
}
// export default authSaga;