/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'
import {
  missionsUpdateFailed,
  updateMissions,
  postMissionData,
  postMissionDataFailed,
  postMissionDataCompleted,
  updateOrganisations,
  orgUpdateFailed
} from '../Actions'
import {
  POST_MISSION_DATA,
  REFRESH_MISSIONS,
} from '../Actions/ActionTypes'
import axios from 'axios'
import { BASE_URL } from '../config';

function* doRefreshMissions() {
  try {
    // This isn't working, but I think the username is in the session
    const auth = yield select((state) => state.AuthReducer.user.credentials)
    // console.log(auth)
    // yield call(delay, 1000)
    const organisationList = yield call(fetchOrganisations, auth)
    const missionList = yield call(fetchMissions, auth)
    const payload = { missionList, organisationList }

    yield put(updateMissions(payload))
  } catch (e) {
    yield put(missionsUpdateFailed())
  }
}

function fetchMissions(auth) {
  return axios.get(BASE_URL + '/mobileapi/user/missions', { auth: auth })
    .then((result) => result.data)
}

function fetchOrganisations(auth) {
  return axios.get(BASE_URL + '/mobileapi/user/organisations', { auth: auth })
    .then((result) => result.data)
}

export default function* root() {
  yield takeEvery(REFRESH_MISSIONS, doRefreshMissions)
}

/*
PUT /webapi/mobileapi/user/missions/MISSIONID/status
{"status": "COMPLETED", "message": "SOME TEXT", "sorties": [{"number": 1, "status": "COMPLETED", "startTime": "2017-12-15T10:00:00Z", "endTime": "2017-12-15T10:00:00Z"}]}
This is a io.flyfreely.portal.services.missions.UpdateMissionStatusCommand object so check there for more details
*/