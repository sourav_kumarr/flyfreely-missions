/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import * as types from './ActionTypes'

const emailChanged = (text) => {
    return {
        type: types.EMAIL_CHANGED,
        payload: text
    }
}

const passwordChanged = (text) => {
    return {
        type: types.PASSWORD_CHANGED,
        payload: text
    }
}

const orgSelected = (text) => {
    return {
        type: types.ORG_SELECTED,
        payload: text
    }
}

const loginUser = (email: string, password) => {
    return { type: types.LOGIN_USER, payload: { email: email, password: password } }
}

const loginUserFail = () => {
    return { type: types.LOGIN_USER_FAIL }
}

const loginUserSuccess = (credentials, user) => {
    return {
        type: types.LOGIN_USER_SUCCESS,
        payload: { credentials, user }
    }
}

const logoutUser = () => {
    return {
        type: types.LOGOUT_USER
    }
}

const selectMission = (missionId: number) => {
    return {
        type: types.SELECT_MISSION,
        payload: missionId
    }
}

const showMissionDetails = (missionId: number) => {
    return {
        type: types.SHOW_MISSION,
        payload: missionId
    }
}

const showMissionDocumentation = (missionId: number) => {
    return {
        type: types.SHOW_MISSION_DOCUMENTATION,
        payload: missionId
    }
}

const refreshMissions = () => {
    return {
        type: types.REFRESH_MISSIONS
    }
}

const updateMissions = (missionAndOrgList) => {
    return {
        type: types.MISSIONS_CHANGED,
        payload: missionAndOrgList
    }
}

const updateOrganisations = (orgList) => {
    return {
        type: types.ORG_REFRESHED,
        payload: orgList
    }
}


const missionsUpdateFailed = () => {
    return {
        type: types.MISSIONS_REFRESH_FAILED,
    }
}

const orgUpdateFailed = () => {
    return {
        type: types.ORG_REFRESH_FAILED,
    }
}

const showMissionFailed = () => {
    return {
        type: types.SHOW_MISSION_FAILED,
    }
}

const storeSortie = (missionId: number, sortie) => {
    return {
        type: types.STORE_SORTIE,
        payload: { missionId, sortie }
    }
}

const postMissionData = (missionId: number) => {
    return {
        type: types.POST_MISSION_DATA,
        payload: {
            missionId,
        }
    }
}

const postMissionDataCompleted = (savedMission) => {
    return {
        type: types.POST_MISSION_DATA_COMPLETED,
        payload: savedMission
    }
}

const postMissionDataFailed = (errorMessage) => {
    return {
        type: types.POST_MISSION_DATA_FAILED,
        payload: errorMessage
    }
}


const showCheckList = (checkListName, selctedCheckList, onChecklistCompleted) => {
    return {
        type: types.SHOW_CHECKLIST,
        payload: {
            checkListName,
            selctedCheckList,
            onChecklistCompleted,
        }
    }
}

const storeCurrentLocation = (location) => ({
        type: types.STORE_CURRENT_LOCATION,
        payload: location
    });


const updateDocuments = (docList) => {
    return {
        type: types.DocumentListActions.SHOW_DOCUMENT_LIST,
        payload: docList
    }
}

const filterDocuments = (docList: any) => {
    return {
        type: types.DocumentListActions.FILTER_DOCUMENT_LIST,
        payload: {
            docList
        }
    }
}

const storeMission = (missionId: number, status: string, step: string, checklistResponses: any, formResponses: any) => {
    return {
        type: types.STORE_MISSION,
        payload: {
            missionId, status, step, checklistResponses, formResponses
        }
    }
}

export {
    emailChanged,
    passwordChanged,
    loginUser,
    logoutUser,
    loginUserFail,
    loginUserSuccess,
    selectMission,
    showMissionDetails,
    refreshMissions,
    updateOrganisations,
    orgUpdateFailed,
    updateMissions,
    missionsUpdateFailed,
    showMissionFailed,
    storeSortie,
    postMissionData,
    postMissionDataFailed,
    postMissionDataCompleted,
    showCheckList,
    storeCurrentLocation,
    orgSelected,
    showMissionDocumentation,
    storeMission,
    filterDocuments,
    updateDocuments,
}
