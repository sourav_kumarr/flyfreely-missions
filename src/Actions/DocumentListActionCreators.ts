/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import { DocumentListActions } from './ActionTypes';

export interface ViewDocumentAction {
    type: DocumentListActions.VIEW_DOCUMENT,
    payload: {
        missionId: number, documentId
    }
}
export const viewDocument = (missionId: number, documentId): ViewDocumentAction => ({
    type: DocumentListActions.VIEW_DOCUMENT,
    payload: {
        missionId, documentId
    }
});

export interface DownloadAllFilesAction {
    type: DocumentListActions.DOWNLOAD_ALL_DOCUMENT,
    payload: {
        missionId: number, documents: any
    }
}
export const downloadAllFiles = (missionId: number, documents: any): DownloadAllFilesAction => ({
    type: DocumentListActions.DOWNLOAD_ALL_DOCUMENT,
    payload: {
        missionId, documents
    }
});

export interface DownloadSingleFileAction {
    type: DocumentListActions.DOWNLOAD_SINGLE_FILE,
    payload: {
        missionId: number, documentId: number
    }
}
export const downloadSingleFile = (missionId: number, documentId: number): DownloadSingleFileAction => ({
    type: DocumentListActions.DOWNLOAD_SINGLE_FILE,
    payload: {
        missionId, documentId
    }
});

export interface DownloadFileProgressAction {
    type: DocumentListActions.DOWNLOAD_FILE_PROGRESS,
    payload: {
        _id: number,
        progress: number
    }
}
export const downloadFileProgress = (_id: number, progress: number): DownloadFileProgressAction => ({
    type: DocumentListActions.DOWNLOAD_FILE_PROGRESS,
    payload: {
        _id, progress
    }
});

export interface DownloadFileErrorAction {
    type: DocumentListActions.DOWNLOAD_FILE_ERROR,
    payload: {
        _id: number,
        error: string
    }
}
export const downloadFileError = (_id: number, error: string): DownloadFileErrorAction => ({
    type: DocumentListActions.DOWNLOAD_FILE_ERROR,
    payload: {
        _id, error
    }
})

export interface DownloadFileFinishedAction {
    type: DocumentListActions.DOWNLOAD_FILE_FINISHED,
    payload: {
        _id: number,
        path: string
    }
}
export const downloadFileFinished = (_id: number, path: string): DownloadFileFinishedAction => (
    {
        type: DocumentListActions.DOWNLOAD_FILE_FINISHED,
        payload: {
            _id, path
        }
    });

/**
 * Combine all the action types for use in the reducer
 */
export type DocumentListActionTypes = ViewDocumentAction | DownloadAllFilesAction | DownloadSingleFileAction 
    | DownloadFileProgressAction | DownloadFileErrorAction | DownloadFileFinishedAction;