/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
export const EMAIL_CHANGED = 'email_changed'
export const PASSWORD_CHANGED = 'password_changed'
export const LOGIN_USER_SUCCESS = 'login_user_success'
export const LOGIN_USER_FAIL = 'login_user_fail'
export const LOGIN_USER = 'login_user'
export const LOGOUT_USER = 'logout_user'

export const SELECT_MISSION = 'select_mission'
export const SHOW_MISSION = 'show_mission'
export const SHOW_MISSION_FAILED = 'show_mission_failed'
export const REFRESH_MISSIONS = 'mission_refresh'
export const MISSIONS_CHANGED = 'mission_changed'
export const ORG_CHANGED = 'org_changed'
export const ORG_REFRESHED = 'org_refreshed'
export const ORG_SELECTED = 'org_selected'
export const MISSIONS_REFRESH_FAILED = 'mission_failed'
export const ORG_REFRESH_FAILED = 'org_failed'

export const STORE_MISSION = 'store_mission'
export const STORE_SORTIE = "store_sortie"
export const POST_MISSION_DATA = 'post_mission_data'     // update data on server
export const POST_MISSION_DATA_COMPLETED = 'post_mission_data_completed'     // update mission in the store
export const POST_MISSION_DATA_FAILED = 'post_mission_data_failed'     // update data on server failed

export const SHOW_CHECKLIST = 'show_checklist'

export const STORE_CURRENT_LOCATION = 'store_current_location'

export enum DocumentListActions {
    SHOW_MISSION_DOCUMENTATION = 'show_documentation',
    VIEW_DOCUMENT = 'view_document',
    SHOW_DOCUMENT_LIST = 'show_document_list',
    FILTER_DOCUMENT_LIST = 'filter_document_list',
    DOWNLOAD_SINGLE_FILE = 'download_single_file',
    DOWNLOAD_ALL_DOCUMENT = 'download_all_document',
    DOWNLOAD_FILE_PROGRESS = 'download_file_progress',
    DOWNLOAD_FILE_FINISHED = 'download_file_finished',
    DOWNLOAD_FILE_ERROR = 'download_file_error'
}