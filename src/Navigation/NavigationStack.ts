/*
 * Copyright (c) 2018 by DEC-UAV Pty. Ltd.
 * All rights reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * DEC-UAV Pty Ltd and its suppliers, if any. The intellectual and technical 
 * concepts contained herein are proprietary to DEC-UAV Pty Ltd and its 
 * suppliers and may be covered by Australian and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination of
 * this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from DEC-UAV Pty Ltd.
 * 
 * If you have accessed these files inadvertently, or they are shared with
 * an unauthorised party you must not disclose or use the information 
 * contained in them. In this event please advise us immediately on 
 * +61 7 4402 4069 or admin@flyfreely.io and delete the files from your system.
*/
import { StackNavigator } from 'react-navigation'
import LoginForm from '../Containers/LoginForm'
import MissionList from '../Containers/MissionList'
import MissionDetails from '../Containers/MissionDetails'

import CheckList from '../Containers/CheckList'
import {Button, Image, TouchableHighlight} from "react-native";


const navigator = StackNavigator(
    {
        login: {
            screen: LoginForm,
            navigationOptions: {
                header: null,
            },
        },
        main: {
            screen: StackNavigator(
                {
                    missions: {
                        screen: MissionList,
                        navigationOptions: {
                            title: 'Missions',
                            headerLeft: null
                        },
                    },
                    missionDetail: {
                        screen: MissionDetails,
                        navigationOptions: {
                            headerTitle: 'Missions'
                        },
                    },
                },
                {
                    // headerMode: 'none',
                }
            ),
        },
        checklist: {
            screen: CheckList
        },
    },
    {
        mode: 'modal',
        headerMode: 'none',
    }
)

export default navigator